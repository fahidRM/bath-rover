package rover;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Canvas;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.EmptyBorder;
import javax.swing.JSplitPane;
import java.awt.Color;
import java.awt.Font;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JCheckBox;
import javax.swing.JTextPane;

import java.awt.event.*;

import java.util.Arrays;

public class RoverWorldView extends JFrame implements GridChangeListener {

    private static final long serialVersionUID = 1L;
    private static final int MIN_TILE_SIZE = 10;

    private GridCanvas canvas =null;
    private JScrollBar hBar;
    private JTextPane txtpnResourcesGold ;
    private JScrollBar vBar;
    private JCheckBox cbxPersist;

    // tells us the part of the GRID we are seeing on the UI (for very large maps)
    private int xAxisOffset;
    private int yAxisOffset;

    private ReadonlyGrid grid;
    private RoverWorldViewListener listener;


    private String generalInfoMessage = "";
    private String tileInfoMessage = "";




    public void setGeneralMessage (String message) {
        generalInfoMessage = message;
    }

    public void setTileInfo (String message) {
        tileInfoMessage =  message;
    }





    public RoverWorldView(ReadonlyGrid g, RoverWorldViewListener l, int sId, String ver, boolean fullscreen) {
        this(sId, ver,  fullscreen);
        grid =  g;
        listener = l;
        g.registerListener(this);
    }


    private String getTileInfo(int xPos, int yPos) {
        return null;
    }

    private int[] getCellSize() {
        final double canvasHeight =  this.canvas.getSize().getHeight();
        final double canvasWidth  = this.canvas.getSize().getWidth();
        int tileWidth = (int) (canvasWidth / grid.getWidth());
        int tileHeight =  (int) (canvasHeight / grid.getHeight());
        tileWidth =  tileWidth < MIN_TILE_SIZE ? MIN_TILE_SIZE :  tileWidth;
        tileHeight =  tileHeight < MIN_TILE_SIZE ? MIN_TILE_SIZE : tileHeight;
        return new int[] { tileWidth, tileHeight };
    }

    private RoverWorldView(int s, String ver, boolean fullscreen) {

        //TODO: refactor where this warning is generated from.... temporary fix
        String warning = "[UNGRADED SCENARIO: only scenarios 1 - 5 are graded]";
        String title = "Rover World (" +  ver + "):- Scenario #" +  s;

        if ((s < 1) || (s > 5)) {
            title =  title + "|" + warning;
        }


        setTitle(title);



        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (fullscreen) {
            setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else {
            setBounds(100, 100, 800, 600);

        }

        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JSplitPane splitPane = new JSplitPane();
        splitPane.setForeground(Color.WHITE);
        contentPane.add(splitPane, BorderLayout.CENTER);

        JPanel gridPanel = new JPanel();
        JPanel optsPanel = new JPanel();


        optsPanel.setBackground(Color.WHITE);
        optsPanel.setSize(400, splitPane.getHeight());
        gridPanel.setLayout(new BorderLayout(0, 0));

        splitPane.setLeftComponent(optsPanel);
        splitPane.setRightComponent(gridPanel);



        GridBagLayout gbl_optsPanel = new GridBagLayout();
        gbl_optsPanel.columnWidths = new int[]{101, 0};
        gbl_optsPanel.rowHeights = new int[]{23, 0, 0};
        gbl_optsPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_optsPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        optsPanel.setLayout(gbl_optsPanel);

        hBar = new JScrollBar();
        hBar.setOrientation(JScrollBar.HORIZONTAL);
        gridPanel.add(hBar, BorderLayout.SOUTH);

        vBar = new JScrollBar();
        gridPanel.add(vBar, BorderLayout.EAST);

        cbxPersist = new JCheckBox();
        cbxPersist.setText("Persist scan region");
        GridBagConstraints gbc_cbxPersist = new GridBagConstraints();
        gbc_cbxPersist.fill  = GridBagConstraints.BOTH;
        gbc_cbxPersist.gridx = 0;
        gbc_cbxPersist.gridy =  2;
        optsPanel.add(cbxPersist, gbc_cbxPersist);

        cbxPersist.addItemListener(new PersistOptionChangeHandler());

        canvas = new GridCanvas(hBar, vBar);
        gridPanel.add(canvas, BorderLayout.CENTER);
        hBar.addAdjustmentListener(canvas);
        vBar.addAdjustmentListener(canvas);
        txtpnResourcesGold = new JTextPane();
        txtpnResourcesGold.setEditable(false);
        txtpnResourcesGold.setText("Information:\n Loading....");
        GridBagConstraints gbc_txtpnResourcesGold = new GridBagConstraints();
        gbc_txtpnResourcesGold.fill = GridBagConstraints.BOTH;
        gbc_txtpnResourcesGold.gridx = 0;
        gbc_txtpnResourcesGold.gridy = 1;
        optsPanel.add(txtpnResourcesGold, gbc_txtpnResourcesGold);
        canvas.addMouseMotionListener(new MouseHandler());


        this.show();
        this.refresh();
    }

    public void updateInfo (String tileInfo) {

        StringBuilder info =   new StringBuilder ("");
        info.append(generalInfoMessage);
        info.append("\n");
        if (tileInfoMessage.trim().length() > 0) {
            info.append("Tile Info:-\n");
            info.append(tileInfoMessage);
        }
        txtpnResourcesGold.setText(info.toString());
    }






    public void endSim (String message) {
        canvas.endSim(message);
    }

    public void onGridTileItemChange (int xPos, int yPos, int[] items, int[] itemsCount) {

        //canvas.drawTile(xPos, yPos, true, items, itemsCount);

    }

    public void onGridTileItemQuantityChange (int xPos, int yPos, int[] items, int[] itemsCount) {

    }

    /**
     * refreshes the UI
     */
    public void refresh () {
        updateInfo (null);
        canvas.repaint();
    }

    private class MouseHandler implements MouseMotionListener {
        public void mouseDragged(MouseEvent e) { }
        public void mouseMoved(MouseEvent e) {
            int[] cellSize =  RoverWorldView.this.getCellSize();

            int col = e.getX() / cellSize[0];
            int lin = e.getY() / cellSize[1];
            if (col >= 0 && lin >= 0 && col < RoverWorldView.this.grid.getWidth() && lin < RoverWorldView.this.grid.getHeight()) {
                try {
                    //String update =  model.getTileInfo(col + xAxisOffset, lin + yAxisOffset);
                   // RoverWorldView.this.updateInfo(update);
                    int xPos =  col +  xAxisOffset;
                    int yPos =  lin +  yAxisOffset;
                    if (listener != null) {
                        listener.onTileSelected(xPos, yPos);
                    }
                }catch (Exception ex) {}
            }
        }
    }
    private class PersistOptionChangeHandler implements ItemListener {
        @Override
        public void itemStateChanged(ItemEvent e) {
            boolean isSelected =  e.getStateChange() == ItemEvent.SELECTED;
            if (listener != null) {
                listener.onPersistRegionToggled(isSelected);
            }

        }
    }



    private class GridCanvas extends Canvas implements AdjustmentListener{

        private boolean hasEnded =  false;
        private JScrollBar hBar;
        private int hBarPos;
        private static final long serialVersionUID = 1L;
        private JScrollBar vBar;
        private int vBarPos;
        private String endMessage;


        public GridCanvas (JScrollBar hb, JScrollBar vb) {
            super();
            vBar =  vb;
            hBar =  hb;
        }


        public void adjustmentValueChanged(AdjustmentEvent evt) {
            Adjustable source = evt.getAdjustable();
            if (evt.getValueIsAdjusting()) {
                return;
            }
            int value = evt.getValue();
            int orient = source.getOrientation();
            if (orient == Adjustable.HORIZONTAL) {
                hBarPos = value;
            } else {
                vBarPos = value;
            }
            this.repaint();
        }

        public void endSim (String message) {
            hasEnded =  true;
            endMessage = message;
            this.repaint();
        }

        private int[] getPecentageShown () {

            final double canvasHeight = this.getSize().getHeight();
            final double canvasWidth =  this.getSize().getWidth();
            final int[] cellSize =  getCellSize();

            final int fullGridHeight =  cellSize[1] * RoverWorldView.this.grid.getHeight();
            final int fullGridWidth =  cellSize[0] * RoverWorldView.this.grid.getWidth();

            int percentHeightShown = (int) ((canvasHeight / fullGridHeight) * 100);
            int percentWidthShown =  (int) ((canvasWidth / fullGridWidth) * 100);

            if (percentHeightShown > 100) { percentHeightShown = 100; }
            if (percentWidthShown >  100) { percentWidthShown = 100; }

            return new int[] {percentWidthShown, percentHeightShown};
        }


        private int[] getStartAndStopLocations (int[] percentageShown) {

            // what percentage are we looking at?
            int hStart = (int)((hBarPos * RoverWorldView.this.grid.getWidth()) / 100);
            int vStart = (int)((vBarPos * RoverWorldView.this.grid.getHeight()) / 100);

            int hStop = (int)( ((hBarPos + percentageShown[0]  ) * RoverWorldView.this.grid.getWidth()) / 100);
            int vStop = (int)(((vBarPos  + percentageShown[1]) * RoverWorldView.this.grid.getHeight()) / 100);

            // fix

            if (percentageShown[1] == 100) { vStart =  0; vStop =  RoverWorldView.this.grid.getHeight(); }
            if (percentageShown[0] == 100) { hStart =  0; hStop =  RoverWorldView.this.grid.getWidth(); }

            return new int[] { hStart, vStart, hStop, vStop };
        }

        public void paint(Graphics g) {
            if (hasEnded) {
                g.clearRect(0, 0, this.getSize().width, this.getSize().height);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setFont( new Font(g2d.getFont().getName(), Font.PLAIN, 14));
                g2d.drawString("End of \n Scenario", 10, 10);
                g2d.drawString("Reason: " +  endMessage, 10, 40);
            }else {
               int[] cellSize =  getCellSize();
               int[] percentageShown = getPecentageShown();

               if (percentageShown[0] == 100) {
                   hBar.setVisible(Boolean.FALSE);
               } else {
                   hBar.setVisible(Boolean.TRUE);
                   hBar.setUnitIncrement(cellSize[0]);
                   hBar.getModel().setExtent(percentageShown[0]);
               }
                // manage bar
                if (percentageShown[1] == 100) {
                    vBar.setVisible(Boolean.FALSE);
                } else {
                    vBar.setVisible(Boolean.TRUE);
                    vBar.setUnitIncrement(cellSize[1]);
                    vBar.getModel().setExtent(percentageShown[1]);
                }

                // draw grid lines
                g.clearRect(0, 0, this.getSize().width, this.getSize().height);
                g.setColor(Color.lightGray);

                for (int l = 1; l < this.getSize().getHeight(); l++) {
                    g.drawLine(0, l * cellSize[1], (int)this.getSize().getWidth() * cellSize[0], l * cellSize[1]);
                }
                for (int c = 1; c <= this.getSize().getWidth(); c++) {
                    g.drawLine(c * cellSize[0], 0, c * cellSize[0], (int) this.getSize().getHeight() * cellSize[1]);
                }


                int[] sasLocation =  getStartAndStopLocations(percentageShown);

                /*
                xAxisOffset =  hStart;
                yAxisOffset =  vStart;
                */
                for (int x = sasLocation[0]; x < sasLocation[2] ; x++) {
                    for (int y = sasLocation[1]; y < sasLocation[3] ; y++) {

                        final int visibleGridX =  x - sasLocation[0];
                        final int visibleGridY =  y - sasLocation[1];

                        drawTile(g, x, y, false);
                    }
                }

            }
        }




        private void drawTile (Graphics g, int gridX, int gridY, boolean drawEmpty) {
            int[] sasLocation = getStartAndStopLocations(getPecentageShown());
            final int visibleGridX =  gridX - sasLocation[0];
            final int visibleGridY =  gridY - sasLocation[1];

            if (! grid.isFree(gridX, gridY)) {
                int[] gridItems =  grid.getItems(gridX, gridY);
                if (gridItems.length > 0) {
                    final int[] itemCopy =  new int[gridItems.length];
                    for (int iii = 0; iii < itemCopy.length; iii++) {
                        itemCopy[iii] = gridItems[iii];
                    }
                   Arrays.sort(itemCopy);
                    for (int i : itemCopy) {

                        if (i == RoverWorldModel.GridItem.BASE.getCode()) {
                            drawBase(g, visibleGridX, visibleGridY);
                        } else if (i == RoverWorldModel.GridItem.AGENT.getCode()) {
                            drawAgent(g, visibleGridX, visibleGridY);
                        } else if (i == RoverWorldModel.GridItem.SCAN_REGION.getCode()) {
                            drawScanRegion(g, visibleGridX, visibleGridY, grid.getCount(RoverWorldModel.GridItem.SCAN_REGION.getCode(), gridX, gridY));
                        } else if (i == RoverWorldModel.GridItem.OBSTACLE.getCode()) {
                            drawObstacle(g, visibleGridX, visibleGridY);
                        }else if (i == RoverWorldModel.GridItem.GOLD.getCode()) {
                            drawResource(g, visibleGridX, visibleGridY, RoverWorldModel.GridItem.GOLD.getCode());
                        } else if (i == RoverWorldModel.GridItem.DIAMOND.getCode()) {
                            drawResource(g, visibleGridX, visibleGridY, RoverWorldModel.GridItem.DIAMOND.getCode());
                        }
                    }
                } else {
                    // draw an empty grid here
                    if (drawEmpty) {
                        drawEmptyCell(g,visibleGridX, visibleGridY);
                    }

                }

            }
        }

        private void drawTile (int x, int y, boolean drawEmpty, int[] tileContents, int[] tileContentQuantities) {
            int[] sasLocation = getStartAndStopLocations(getPecentageShown());
            final int visibleGridX =  x - sasLocation[0];
            final int visibleGridY =  y - sasLocation[1];
            Graphics g =  this.getGraphics();


                int[] gridItems =  tileContents;
                if (gridItems.length > 0) {
                    final int[] itemCopy =  new int[gridItems.length];
                    for (int iii = 0; iii < itemCopy.length; iii++) {
                        itemCopy[iii] = gridItems[iii];
                    }
                    Arrays.sort(itemCopy);
                    for (int i : itemCopy) {
                        if (i == RoverWorldModel.GridItem.BASE.getCode()) {
                            drawBase(g, visibleGridX, visibleGridY);
                        } else if (i == RoverWorldModel.GridItem.AGENT.getCode()) {
                            drawAgent(g, visibleGridX, visibleGridY);
                        } else if (i == RoverWorldModel.GridItem.SCAN_REGION.getCode()) {
                            drawScanRegion(g, visibleGridX, visibleGridY, grid.getCount(RoverWorldModel.GridItem.SCAN_REGION.getCode(), x, y));
                        } else if (i == RoverWorldModel.GridItem.OBSTACLE.getCode()) {
                            drawObstacle(g, visibleGridX, visibleGridY);
                        } else if (i == RoverWorldModel.GridItem.GOLD.getCode()) {
                            drawResource(g, visibleGridX, visibleGridY, RoverWorldModel.GridItem.GOLD.getCode());
                        } else if (i == RoverWorldModel.GridItem.DIAMOND.getCode()) {
                            drawResource(g, visibleGridX, visibleGridY, RoverWorldModel.GridItem.DIAMOND.getCode());
                        }
                    }
                } else {
                    if (drawEmpty) {
                        drawEmptyCell(g,visibleGridX, visibleGridY);
                    }
                }


        }


        /**
         * Draws the scan region on the Map
         * @param g
         * 				Graphics obj
         * @param x
         * 				x-axis location of scan range centre
         * @param y
         * 				y-axis location of scan range centre
         * @param r
         * 				radius of scan range
         */
        private void drawScanRegion (Graphics g, int x, int y, int r) {

            int[] cellSize =  RoverWorldView.this.getCellSize();
            int x1 = x - r;
            int y1 =  y -r;
            int x2 =  x + r;
            int y2 =  y + 2;
            int actualX =  (x - r);
            int actualY = (y - r);
            int actualW =  ((2 * r) + 1 ) * cellSize[0];
            int actualH = ((2 * r) +  1) * cellSize[1];
            g.setColor(new Color(155,229,155, 35));
            g.fillOval( (actualX * cellSize[0])  , (actualY * cellSize[1]), actualW, actualH);

            // manages situations where the scan range is cut off by the border of the grid
            // crudely
            if (x1 < 0 && y1 < 0) {
                int newX1 =  RoverWorldView.this.grid.getWidth() + x1;
                int newY1 =  RoverWorldView.this.grid.getHeight() + y1;
                g.fillOval( (newX1 * cellSize[0])  , (newY1 * cellSize[1]), actualW, actualH);

            } if ( x1 < 0) {
                int newX1 =  RoverWorldView.this.grid.getWidth() + x1;
                g.fillOval( (newX1 * cellSize[0])  , (actualY * cellSize[1]), actualW, actualH);

            } if ( y1 < 0) {
                int newY1 =  RoverWorldView.this.grid.getHeight() + y1;
                g.fillOval( (actualX * cellSize[0])  , (newY1 * cellSize[1]), actualW, actualH);

            }


            if ( (x2 > RoverWorldView.this.grid.getWidth()) && ( y2 > RoverWorldView.this.grid.getHeight())) {
                int newX2 =  0 - (RoverWorldView.this.grid.getWidth() - actualX);
                int newY2 =  0 - (RoverWorldView.this.grid.getHeight() - actualY);
                g.fillOval( (newX2 * cellSize[0])  , (newY2 * cellSize[1]), actualW, actualH);

            }  if (x2 > RoverWorldView.this.grid.getWidth()) {
                int newX2 =  0 - (RoverWorldView.this.grid.getWidth() - actualX);
                g.fillOval( (newX2 * cellSize[0])  , (actualY * cellSize[1]), actualW, actualH);
            } if ( y2 > RoverWorldView.this.grid.getHeight()) {
                int newY2 =  0 - (RoverWorldView.this.grid.getHeight() - actualY);
                g.fillOval( (actualX * cellSize[0])  , (newY2 * cellSize[1]), actualW, actualH);

            }

            int endX =  actualX +  r + r;
            int endY =  actualY + r + r;

            if (endX > RoverWorldView.this.grid.getWidth()) {
                int xOverFlow = (0 - endX + (RoverWorldView.this.grid.getWidth() - endX));

                g.fillOval( -1 * (xOverFlow* cellSize[0]), (actualY * cellSize[1]),  actualW,  actualH);
            }

            if (endY > RoverWorldView.this.grid.getHeight()) {
                int yOverFlow = (0 - endY + (RoverWorldView.this.grid.getHeight() - endY));
                g.fillOval((actualX * cellSize[0]),  -1 * (yOverFlow* cellSize[1]),  actualW,  actualH);
            }

            if (actualX < 0) {
                int xOverFlow =  RoverWorldView.this.grid.getWidth() - endX ;
                g.fillOval((xOverFlow* cellSize[0]), (actualY * cellSize[1]),  actualW,  actualH);

            }

            if (actualY < 0) {
                int yOverFlow =  RoverWorldView.this.grid.getHeight() - endY;
                g.fillOval((actualX * cellSize[0]),   (yOverFlow* cellSize[1]),  actualW,  actualH);
            }
        }

        /**
         * Draws A base
         * @param g
         * 				Graphics Obj
         * @param x
         * 				x-axis location
         * @param y
         * 				y-axis location
         */
        private void drawBase (Graphics g, int x, int y) {
            int[] cellSize =  RoverWorldView.this.getCellSize();

            g.setColor(Color.gray);
            g.fillRect(x * cellSize[0], y * cellSize[1], cellSize[0], cellSize[1]);
            g.setColor(Color.pink);
            g.drawRect(x * cellSize[0] + 2, y * cellSize[1] + 2, cellSize[0] - 4,
                    cellSize[1] - 4);
            g.drawLine(x * cellSize[0] + 2, y * cellSize[1] + 2, (x + 1) * cellSize[0]
                    - 2, (y + 1) * cellSize[1] - 2);
            g.drawLine(x * cellSize[0] + 2, (y + 1) * cellSize[1] - 2, (x + 1)
                    * cellSize[0] - 2, y * cellSize[1] + 2);
        }


        private void drawEmptyCell (Graphics g, int x, int y) {
            int[] cellSize =  RoverWorldView.this.getCellSize();
            g.clearRect(x * cellSize[0], y * cellSize[1], cellSize[0], cellSize[1]);
        }

        /**
         * Draws An Agent
         * @param g
         * 				Graphics Object
         * @param x
         * @param y
         */
        private void drawAgent (Graphics g, int x, int y) {
            int[] cellSize =  RoverWorldView.this.getCellSize();
            int fivePH = (int)(5 * cellSize[1] / 100);
            int fivePW = (int)(5 * cellSize[0] / 100);
            int ninetyPH =  (int) ( 80 * cellSize[1] / 100);
            int ninetyPW =  (int) ( 80 * cellSize[0] / 100);
            g.setColor(Color.red);
            g.fillOval( (x * cellSize[0]) +  fivePW, (y * cellSize[1]) +  fivePH, ninetyPW, ninetyPH);
        }

        /**
         * Draws a resource
         *
         * @param g
         * 				Graphics object
         * @param x
         * 				x-axis location
         * @param y
         * 				y-axis location
         * @param t
         * 				Resource type
         */
        private void drawResource (Graphics g, int x, int y, int t) {
            int[] cellSize =   RoverWorldView.this.getCellSize();
            if (t == RoverWorldModel.GridItem.GOLD.getCode()) {
                g.setColor(Color.yellow);
            } else {
                g.setColor(Color.blue);
            }
            g.drawRect(
                    x * cellSize[0] + 2,
                    y * cellSize[1] + 2,
                    cellSize[0] - 4,
                    cellSize[1] - 4);
            final int[] vx = new int[4];
            final int[] vy = new int[4];
            vx[0] = x * cellSize[0] + (cellSize[0] / 2);
            vy[0] = y * cellSize[1];
            vx[1] = (x + 1) * cellSize[0];
            vy[1] = y * cellSize[1] + (cellSize[1] / 2);
            vx[2] = x * cellSize[0] + (cellSize[0] / 2);
            vy[2] = (y + 1) * cellSize[1];
            vx[3] = x * cellSize[0];
            vy[3] = y *cellSize[1] + (cellSize[1] / 2);
            g.fillPolygon(vx, vy, 4);
        }



        private void drawObstacle (Graphics g, int x, int y) {
            int[] cellSize =  RoverWorldView.this.getCellSize();

            g.setColor(Color.black);
            g.fillRect(x * cellSize[0], y * cellSize[1], cellSize[0], cellSize[1]);
        }


    }


}
