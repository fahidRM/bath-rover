package rover.util;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOError;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rover.Scenario;

public class ScenarioParser {

    /** JSON DOC KEYS **/
    private static final String ENVIRONMENT_KEY = "environment";
    private static final String META_KEY = "meta";
    private static final String SCENATIOS_KEY = "scenarios";

    /** ENVIRONMENT OBJECT KEYS **/
    private static final String ENERGY_KEY = "energy";
    private static final String COS_PER_RC_KEY = "cost_per_reasoning_cycle";

    /** META OBJECT KEYS **/
    private static final String VERSION_KEY = "version";

    /** SCENARIO OBJECT KEYS **/
    private static final String AGENT_COUNT_KEY = "ag_count";
    private static final String AGENTS_KEY = "agents";
    private static final String BASE_KEY = "base";
    private static final String ENERGY_CATEGORY_KEY =  "energy_category";
    private static final String FULLSCREEN_KEY = "full_screen";
    private static final String HEIGHT_KEY = "height";
    private static final String ID_KEY = "id";
    private static final String OBSTACLE_DENSITY_KEY = "obstacle_density";
    private static final String OBSTACLES_KEY = "obstacles";
    private static final String QTY_KEY  = "qty";
    private static final String RESOURCE_COUNT_KEY = "rs_count";
    private static final String RESOURCE_DISTRIBUTION_KEY = "rs_dist";
    private static final String RESOURCES_KEY = "resources";
    private static final String RESOURCE_TYPE_KEY = "rs_types";
    private static final String TYPE_KEY = "type";
    private static final String WIDTH_KEY = "width";
    private static final String X_KEY =  "x";
    private static final String Y_KEY =  "y";

    /**
     * Parses a scenario definition and returns it as a Scenario object
     *
     * @param json  JSON string or fiel path to Scenario definintion
     * @param id    ID of the specific scenario to read
     * @boolean isPath specifies if the json variable is a string or a file path
     * @return
     *
     * @apiNote Rather than parse the entire document and retrieve all defined scenario, this would focus on selecting only a single scenario as there is no support for changing scenario on the fly.
     */
    public  static Scenario parse (String json, int id, boolean isPath) throws JSONException, IOException {
        String jsonString =  json;
            // Read file content if file path was provided
            if (isPath) {
                final BufferedReader fileReader = new BufferedReader(new FileReader(json));
                final StringBuilder jsonContent = new StringBuilder();
                String line;
                while ((line =  fileReader.readLine()) != null) {
                    jsonContent.append(line);
                }
                fileReader.close();
                jsonString = jsonContent.toString();
            }

            final JSONObject jsonDoc = new JSONObject(jsonString);

            final JSONObject metadata = jsonDoc.getJSONObject(META_KEY);
            final String scenariosVersion = metadata.getString(VERSION_KEY);

            final JSONObject environmentData = jsonDoc.getJSONObject(ENVIRONMENT_KEY);
            final JSONObject energyData =  environmentData.getJSONObject(ENERGY_KEY);
            final double energyCostPerCycle =  energyData.getDouble(COS_PER_RC_KEY);

            final JSONArray scenarios = jsonDoc.getJSONArray(SCENATIOS_KEY);
            for (int i = 0; i < scenarios.length(); i++) {
                final JSONObject scenario =  (JSONObject) scenarios.get(i);
                int scenarioId = scenario.getInt(ID_KEY);
                if (scenarioId == id) {

                    boolean isARamdonMap = scenario.has(TYPE_KEY);
                    boolean foundAllKeysForSimpleMapSpecification = scenario.has(AGENT_COUNT_KEY) && scenario.has(RESOURCE_COUNT_KEY) && scenario.has(RESOURCE_DISTRIBUTION_KEY) && scenario.has(OBSTACLE_DENSITY_KEY);
                    boolean foundAllKeysForComplexMapSpecification = scenario.has(BASE_KEY) && scenario.has(RESOURCES_KEY) && scenario.has(AGENTS_KEY);
                   if (isARamdonMap) {
                        boolean hasAllRandomKeys = scenario.has(FULLSCREEN_KEY) && scenario.has(ENERGY_CATEGORY_KEY)
                                && scenario.has(OBSTACLE_DENSITY_KEY) && scenario.has(RESOURCE_TYPE_KEY)
                                && scenario.has(AGENT_COUNT_KEY);

                        if (hasAllRandomKeys) {

                            boolean launchOnFullscreen = false;
                            if ((scenario.getString(FULLSCREEN_KEY).toLowerCase().equals("yes"))) {
                                launchOnFullscreen = true;
                            }

                            int[] rts = new int[2];
                                JSONArray rtArray = scenario.getJSONArray(RESOURCE_TYPE_KEY);
                                rts =  new int[rtArray.length()];
                                for (int k = 0; k < rtArray.length(); k++) {
                                    rts[k] = rtArray.getInt(k);
                                }

                                    return new Scenario(
                                            scenarioId,
                                    launchOnFullscreen,
                                    scenario.getString(ENERGY_CATEGORY_KEY),
                                    scenario.getString(OBSTACLE_DENSITY_KEY),
                                    rts,
                                    scenario.getInt(AGENT_COUNT_KEY),
                                            energyCostPerCycle,
                                            scenariosVersion

                            );


                        }else {
                            throw new JSONException("Missing keys in scenario definition: scenario#" + scenarioId );
                        }
                    } else {
                        String energyCategory =  scenario.getString(ENERGY_CATEGORY_KEY);
                        int height =  scenario.getInt(HEIGHT_KEY);
                        int width =  scenario.getInt(WIDTH_KEY);

                        // search for specific keys to figure out if it's a standard scenario, detailed scenario or invalid one

                        if (foundAllKeysForSimpleMapSpecification) {
                            int[] rts = new int[2];
                            if (scenario.has(RESOURCE_TYPE_KEY)) {
                                JSONArray rtArray = scenario.getJSONArray(RESOURCE_TYPE_KEY);
                                rts =  new int[rtArray.length()];
                                for (int k = 0; k < rtArray.length(); k++) {
                                    rts[k] = rtArray.getInt(k);
                                }
                            }

                            boolean launchOnFullscreen = false;
                            if (scenario.has(FULLSCREEN_KEY) && (scenario.getString(FULLSCREEN_KEY).toLowerCase().equals("yes"))) {
                                launchOnFullscreen = true;
                            }

                            return new Scenario(
                                    scenarioId,
                                    scenario.getInt(AGENT_COUNT_KEY),
                                    energyCategory,
                                    height,
                                    scenario.getInt(RESOURCE_COUNT_KEY),
                                    scenario.getInt(RESOURCE_DISTRIBUTION_KEY),
                                    rts,
                                    width,
                                    scenario.getString(OBSTACLE_DENSITY_KEY),
                                    scenariosVersion,
                                    energyCostPerCycle,
                                    launchOnFullscreen
                            );
                        }
                        else if (foundAllKeysForComplexMapSpecification) {
                            JSONArray agentsArray = scenario.getJSONArray(AGENTS_KEY);
                            JSONArray resourcesArray = scenario.getJSONArray(RESOURCES_KEY);
                            JSONArray obstaclesArray = scenario.getJSONArray(OBSTACLES_KEY);
                            JSONObject baseObj = scenario.getJSONObject(BASE_KEY);


                            int[] agx = new int[agentsArray.length()];
                            int[] agy = new int[agentsArray.length()];
                            int[] obx = new int[obstaclesArray.length()];
                            int[] oby = new int[obstaclesArray.length()];
                            int[] rsc = new int[resourcesArray.length()];
                            int[] rst = new int[resourcesArray.length()];
                            int[] rsx = new int[resourcesArray.length()];
                            int[] rsy = new int[resourcesArray.length()];
                            int[] base = new int[2];

                            if ( !(baseObj.has(X_KEY) && baseObj.has(Y_KEY))) {   throw new JSONException("Incomplete base location: scenario# " + scenarioId); }
                            base[0] = baseObj.getInt(X_KEY);
                            base[1] = baseObj.getInt(Y_KEY);

                            for (int jj = 0; jj < agentsArray.length(); jj++) {
                                JSONObject agentSpec =  agentsArray.getJSONObject(jj);
                                if (! (agentSpec.has(X_KEY) && agentSpec.has(Y_KEY))) {
                                    throw new JSONException("Incomplete agent location: scenario#" + scenarioId);
                                }
                                agx[jj] =  agentSpec.getInt(X_KEY);
                                agy[jj] = agentSpec.getInt(Y_KEY);

                            }

                            for (int kk = 0; kk < resourcesArray.length(); kk++) {
                                JSONObject rsSpec =  resourcesArray.getJSONObject(kk);
                                if (! (rsSpec.has(X_KEY) && (rsSpec.has(Y_KEY)) && rsSpec.has(QTY_KEY) && rsSpec.has(TYPE_KEY))) {
                                    throw new JSONException("Incomplete resource definition: scenario#" + scenarioId);
                                }
                                rsc[kk] = rsSpec.getInt(QTY_KEY);
                                rst[kk] = rsSpec.getInt(TYPE_KEY);
                                rsx[kk] = rsSpec.getInt(X_KEY);
                                rsy[kk] = rsSpec.getInt(Y_KEY);
                            }

                            for (int ll = 0; ll < obstaclesArray.length(); ll++) {
                                JSONObject obSpec = obstaclesArray.getJSONObject(ll);
                                if (! (obSpec.has(X_KEY) && obSpec.has(Y_KEY))) {
                                    throw new JSONException("Incomplete obstale definition: scenario#" +  scenarioId);
                                }
                                obx[ll] =  obSpec.getInt(X_KEY);
                                oby[ll] =  obSpec.getInt(Y_KEY);
                            }

                            boolean launchOnFullscreen = false;
                            if (scenario.has(FULLSCREEN_KEY) && (scenario.getString(FULLSCREEN_KEY).toLowerCase().equals("yes"))) {
                                launchOnFullscreen = true;
                            }

                            return new Scenario(
                                    scenarioId, energyCategory, height, width,
                                    base, agx, agy, obx, oby, rsc, rst, rsx, rsy,
                                    scenariosVersion,
                                    energyCostPerCycle,
                                    launchOnFullscreen
                            );
                        }
                        else {
                            throw new JSONException("Missing keys in scenario definition: scenario#" + scenarioId );
                        }
                    }
                }
                continue;
            }




        return null;
    }

}
