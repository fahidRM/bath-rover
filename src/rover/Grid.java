package rover;


import java.lang.System;
import java.util.ArrayList;

public class Grid implements ReadonlyGrid {
    // items on grid and their amount
    private int [][][] items;
    private int [][][] itemsCount;
    // Grid dimension
    private final int height; // y-axis
    private final int width; // x-axis
    // Grid listeners
    private ArrayList<GridChangeListener> listeners;

    /**
     * Constructor
     * @param gridWidth     Height of grid to create
     * @param gridHeight    Width of grid to create
     */
    public Grid (int gridWidth, int gridHeight) {
        listeners =  new ArrayList<>();
        width = gridWidth;
        height =  gridHeight;
        items = new int[gridWidth][gridHeight][];
        itemsCount =  new int[gridWidth][gridHeight][];

        for (int xpos = 0; xpos < width; xpos ++) {
            for (int ypos = 0; ypos < height; ypos++) {
                items[xpos][ypos] = new int[]{};
                itemsCount[xpos][ypos] = new int[]{};
            }
        }
    }

    /**
     *
     * @param item      item to add on map
     * @param xpos      x-axis location of tile where item is to be added
     * @param ypos      y-axis location of tile where item is to be added
     * @param quantity  quantity of item to add
     *
     * @apiNote xpos, ypos and quantity must be unsigned integers
     */
    public void addItem (int item, int xpos, int ypos, int quantity) {
        if ((quantity > 0) && inGrid(xpos, ypos)) {
            int itemIndex =  getItemIndex (item, xpos, ypos);
            if (itemIndex > -1) {
                itemsCount[xpos][ypos][itemIndex] = itemsCount[xpos][ypos][itemIndex] + quantity;
                updateListeners(xpos, ypos, true);
            } else {
                int[] newItems =  new int [items[xpos][ypos].length + 1];
                newItems[items[xpos][ypos].length] = item;
                System.arraycopy(items[xpos][ypos], 0, newItems, 0, items[xpos][ypos].length);

                int[] newItemsCount =  new int [newItems.length];
                newItemsCount[newItemsCount.length -1] = quantity;
                System.arraycopy(itemsCount[xpos][ypos], 0, newItemsCount, 0, itemsCount[xpos][ypos].length);

                items[xpos][ypos] =  newItems;
                itemsCount[xpos][ypos] =  newItemsCount;
                updateListeners(xpos, ypos, false);
            }
        }

    }

    public void addOrUpdateItemIfLarger (int item, int xpos, int ypos, int quantity) {

        int index =  getItemIndex(item, xpos, ypos);
        if (index > -1) {
            int count = itemsCount[xpos][ypos][index];
            if (count < quantity) {
                itemsCount[xpos][ypos][index] =  quantity;
            }
        } else {
            addItem(item, xpos, ypos, quantity);
        }
    }




    /**
     * Returns the total count of an item in the grid.
     * @param item      item to check for (integer)
     * @return          Number of items in the grid (integer)
     */
    public int getCount (int item) {
        int count = 0;
        for (int x = 0; x < width; x++) {
            for (int y =0; y < height; y++) {
                count +=  getCount (item, x, y);
            }
        }
        return count;
    }

    /**
     * Returns the amount of an item in agrid tile.
     * @param item  item to check for (integer).
     * @param xpos  x location of grid tile (unsigned integer).
     * @param ypos  y location of grid tile (unsigned integer).
     * @return Number of item in specified grid tile.
     */
    public int getCount (int item, int xpos, int ypos) {
        int itemIndex = getItemIndex (item, xpos, ypos);
        return itemIndex > -1  ? itemsCount[xpos][ypos][itemIndex] : 0;
    }

    /**
     * Returns the grid's height
     * @return  Grid's height
     */
    public int getHeight () {
        return height;
    }

    /**
     * Returns all items located at a grid tile
     * @param xpos  x-axis location of grid tile to check
     * @param ypos  y-axis location of grid tile to check
     * @return      list of items at grid tile
     *
     * @apiNote retuned array would have a length of 0 if tile is empty
     */
    public int[] getItems (int xpos, int ypos) {
        return inGrid(xpos, ypos) ? items[xpos][ypos] : new int[] {};
    }

    /**
     *
     * @param xpos  x-axis location of grid tile
     * @param ypos  y-axis location of grid tile
     * @return      amount of each item contained in the grid tile
     * @apiNote     Quantities are stored in the same order as items. i.e the first item in the tile's quantity is the first element in the returned array and so on.
     *              This method should be used with {@link Grid#getItems}.
     */
    public int[] getItemsCount (int xpos, int ypos) {
        return inGrid(xpos, ypos) ? itemsCount[xpos][ypos] : new int[] {};
    }

    /**
     * Returns the index of an item in a grid tile
     * @param item  item to search for
     * @param xpos  x-axis location of grid tile
     * @param ypos  y-axis location of grid tile
     * @return  index of item in grid tile
     * @apiNote A response of -1 means that the item is not located in the grid tile.
     */
    private int getItemIndex (int item, int xpos, int ypos) {
        if (inGrid (xpos, ypos)) {
            for (int i = 0; i < items[xpos][ypos].length; i++) {
                if (items[xpos][ypos][i] == item) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the Grid's width
     * @return  Grid's width
     */
    public int getWidth () {
        return width;
    }

    /**
     * Confirms that an item is present in a grid tile
     * @param item  item to search for
     * @param xpos  x-axis location of item
     * @param ypos  y-axis location of item
     * @return  confirmation of the presence/absence of the item
     */
    public boolean hasItem (int item, int xpos, int ypos) {
        return getItemIndex(item, xpos, ypos ) > -1;
    }

    /**
     * Checks that a tile is within the grid
     * @param xpos  x-axis location of grid tile
     * @param ypos  y-axis location of grid tile
     * @return      Confirmation that the tile is withing the grid boundary.
     */
    private boolean inGrid (int xpos, int ypos) {
        return (xpos >= 0) && (ypos >= 0) && (xpos < width) && (ypos < height);
    }

    /**
     * Checks if a grid tile is free
     * @param xpos  x-axis location of grid tile
     * @param ypos  y-axis location of grid tile
     * @return      Confirmation of grid tile emptiness
     */
    public boolean isFree (int xpos, int ypos) {
        return inGrid(xpos, ypos) && (itemsCount[xpos][ypos].length == 0 );
    }

    /**
     * Reduces the amount of an item in a grid tile
     * @param item      item to reduce
     * @param xpos      x-axis location of grid tile
     * @param ypos      y-axis location of grid tile
     * @param quantity  amount to reduce item by
     * @apiNote: xpos, ypos and quantity are expected to be unsigned integers
     *
     */
    public void reduceItem (int item, int xpos, int ypos, int quantity) {
        if ((quantity > 0) && inGrid(xpos, ypos)) {
            int itemIndex = getItemIndex (item, xpos, ypos);
            if (itemIndex > -1) {
                if (itemsCount[xpos][ypos][itemIndex] <= quantity) { removeItem(item, xpos, ypos); }
                else {
                    itemsCount[xpos][ypos][itemIndex] = itemsCount[xpos][ypos][itemIndex] - quantity;
                    updateListeners(xpos, ypos, true);
                }
            }
        }
    }

    /**
     * Removes an item from a grid tile
     * @param item  item to remove
     * @param xpos  x-axis location of grid tile
     * @param ypos  y-axis location of grid tile
     */
    public void removeItem (int item, int xpos, int ypos) {
        if (inGrid(xpos, ypos)) {
            int itemIndex =  getItemIndex(item, xpos, ypos);
            if (itemIndex > -1) {
                if (items[xpos][ypos].length == 1) {
                    items[xpos][ypos] =  new int[] {};
                    itemsCount[xpos][ypos] =  new int[] {};
                } else {
                    int[] newItems =  new int[items[xpos][ypos].length -1];
                    int[] newItemsCount =  new int[items[xpos][ypos].length -1];
                    boolean found =  false;

                    for (int i = 0; i < items[xpos][ypos].length; i++) {
                        if (i != itemIndex) {
                            if (found) {
                                newItems[i - 1] = items[xpos][ypos][i];
                                newItemsCount[i - 1] = itemsCount[xpos][ypos][i];
                            } else {
                                newItems[i] = items[xpos][ypos][i];
                                newItemsCount[i] = itemsCount[xpos][ypos][i];
                            }
                        } else {
                            found =  true;
                        }
                    }
                    items[xpos][ypos] = newItems;
                    itemsCount[xpos][ypos] =  newItemsCount;
                }
                updateListeners(xpos, ypos, false);
            }
        }
    }

    /**
     * Calculate the new location of a map given a displacement was made with wrap enabled
     * @param xpos      x-axis location of grid tile
     * @param ypos      y-axis location of grid tile
     * @param xdisp     x-axis displacement
     * @param ydisp     y-axis displacement
     * @return          new tile location
     * @apiNote         result presented as { newX, newY}
     */
    public int[] getNewLocationWithWrapEnabled ( int xpos, int ypos, int xdisp, int ydisp) {
        int xSum =  xpos + xdisp;
        int ySum =  ypos +  ydisp;
        while (xSum < 0) {     xSum = width + xSum;    }
        while (ySum < 0) {     ySum = height +  ySum;  }
        return new int[] {xSum % width, ySum % height};
    }


    /**
     * Creates a string representation of a grid tile
     * @param xpos  x-axis location of grid tile
     * @param ypos  y-axis location of grid tile
     * @return  String representation of grid tile
     */
    public String stringifyGridTile (int xpos, int ypos) {
        StringBuilder tileValue = new StringBuilder("[");
        for (int i = 0; i < items[xpos][ypos].length; i++) {
            tileValue.append(" " + items[xpos][ypos][i] + ":" + itemsCount[xpos][ypos][i] + "," );
        }
        return tileValue.append("]").toString();
    }


    /**
     * registers a new grid change listener
     * @param listener  gridchange listener
     */
    public void registerListener (GridChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * unregisters a gridchange listener
     * @param listener  gridchange listener
     */
    public void unregisterListener (GridChangeListener listener) {
        if (listeners.indexOf(listener) > -1) { listeners.remove(listener); }
    }

    /**
     * updates all gridchange listeners
     * @param xPos              x-axis location of tile that changed
     * @param yPos              y-axis location of tile that changed
     * @param isQuantityChange  flag to indicate if the tile change was a change in quantity of an item located on it
     */
    private void updateListeners (int xPos, int yPos, boolean isQuantityChange) {
        if (listeners !=  null && listeners.size() > 0) {
            for (GridChangeListener listener : listeners) {
                if (isQuantityChange) {
                    listener.onGridTileItemQuantityChange(xPos, yPos, getItems(xPos, yPos), getItemsCount(xPos, yPos));
                }else {
                    listener.onGridTileItemChange(xPos, yPos, getItems(xPos, yPos), getItemsCount(xPos, yPos));
                }
            }
        }
    }




    /**
     *
     * @return String representation of entire grid
     */
    @Override
    public String toString() {
        StringBuilder map = new StringBuilder("");
        for (int ypos = 0; ypos < height; ypos++) {
            for (int xpos = 0; xpos < width; xpos++) {
                map.append(stringifyGridTile(xpos, ypos));
            }
            map.append("\n");
        }
        return map.toString();
    }

}
