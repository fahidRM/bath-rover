package rover;

public interface RoverWorldModelEndListener {
    void onEndConditionDetected(String reason);
}
