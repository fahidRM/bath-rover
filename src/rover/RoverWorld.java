package rover;

import java.io.*;

import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Structure;
import jason.mas2j.AgentParameters;
import jason.mas2j.MAS2JProject;

import org.json.JSONException;

import java.util.ArrayList;

import rover.util.ScenarioParser;

//import bath.cs.fahidrm.logger.instinct.*;



public class RoverWorld extends StepSynchedEnvironment implements RoverWorldModelEndListener /*implements CPatchListener, ILoggerListener*/ {
    // arguments expected to be passed to the environment
    private static final String[] requiredArgs = new String[]{"mas2j_file", "scenario"};
    private static final String[] externalDebuggerArgs = new String[]{"external_debugger", "external_debug_server_address", "external_debug_server_port"};
    // scenarios definition default file name
    private static final String SCENARIOS_DEFINITION_FILE = "scenarios.json";

    // world model
    private static RoverWorldModel model =  null;
    // feedback for agents actions
    private ActionFeedback[] feedback;
    private boolean[] maskFeedback;

    private String receivedContents = "";

    private boolean iClientShouldListen = true;
    private boolean isStartOfCycle =  true;

    /**
     * Initialises the environment
     * @param args      command line args
     */
    @Override
    public void init(String[] args) {
        super.init(new String[]{"1000"});
        super.setSleep(100);

        try {

            String[] argVals = parseArgs(args);

            // TODO: set up the external debugger here.... argVals [3-5]
            // Parsing the mas2j file
            // TODO: refactor out the value 5. Store as variable just in case we decide to add more params down the line....

            System.out.println("Setting up environment.....");
            String[] informationAboutAgents = parseMas2J(argVals[0]);
            String[] agName =  new String[informationAboutAgents.length / 5];
            int[] agCapacity = new int[agName.length];
            int[] agSpeed = new int[agName.length];
            int[] agRange = new int[agName.length];
            String[] agPrefRes = new String[agName.length];

            feedback = new ActionFeedback[agName.length];
            maskFeedback = new boolean[agName.length];

            for (int i = 0; i < informationAboutAgents.length - 4; i += 5) {

                final int j =  i / 5;

                agName[j] = informationAboutAgents[i];
                agCapacity[j] = Integer.parseInt(informationAboutAgents[i+1]);
                agRange[j] = Integer.parseInt(informationAboutAgents[i+2]);
                agSpeed[j] = Integer.parseInt(informationAboutAgents[i+3]);
                agPrefRes[j] = informationAboutAgents[i+4];

                final int configPoints = agCapacity[j] +  agSpeed[j] + agRange[j];

                if (    // Catching config violations
                        (configPoints > 9)
                                || (agCapacity[j] < 0) || (agSpeed[j] < 0) || (agRange [j] < 0)
                ){
                    agCapacity[i % 5] = 3;
                    agSpeed[i % 5] = 3;
                    agRange[i % 5] = 3;
                    System.out.println ("Invalid configuration detected for Agent: " + agName[j] + ".\n Reconfigured as follows: capacity=3, speed=3, range=3");
                }else if (configPoints < 9) {   // informing about under-configuration ...if that's a word
                    System.out.println ("WARNING: " +  agName[j] + " has not been configured to the max.");
                }
                System.out.println("Configured " + agName[j] + " as: speed=" + agSpeed[j] + " , capacity= "  + agCapacity[j] + " , range= " + agRange[j] + " , pref res= " + agPrefRes[j]);
            }


            Scenario scenario = ScenarioParser.parse(SCENARIOS_DEFINITION_FILE, Integer.parseInt(argVals[1]), true);
            if (scenario == null) {
                System.out.println("Error parsing scenario.\nCould not find definition of Scenario #" +  argVals[1]);
                stop();
            } else {
                //confirm agents count
                if (scenario.getAgCount() < agName.length) {
                    System.out.println("Error: Scenario #" +  scenario.getId()   +  " requires a max of " +  scenario.getAgCount() + " agent(s) but " +  agName.length +  " were specified.");
                    stop();
                } else {
                    model = RoverWorldModel.buildModel(this, scenario, agName, agCapacity, agSpeed, agRange, agPrefRes);
                }
            }
        }catch (JSONException je) {
            System.out.println("Error parsing scenarios file.\nReason: " +  je.getMessage());
            stop();
        }catch (FileNotFoundException fnf) {
            System.out.println("Error: mas2j file not found.\n" + fnf.getMessage());
            stop();
        } catch (jason.mas2j.parser.ParseException pe) {
            System.out.println("Error: parsing mas2j file failed.\n" + pe.getMessage());
            stop();
        } catch (NumberFormatException nfe) {
            System.out.println("Error: could not parse a config entry.\n" +  nfe.getMessage());
            stop();
        } catch (IOException ioe) {
            System.out.println("Error reading scenario file or Parsing the Mas2J file.\nEnsure scenarios.json is present in project's root directory.\nReason: " +  ioe.getMessage());
            stop();
        } catch (Exception e) {
            System.out.println("Well this is strange .... " +  e.getMessage());
            stop();
        }
    }


    public void onLogEntered (String agent, String entry){

    }

    public void onFeedback (String feedback) {

    }

    public void stopListening(){
        iClientShouldListen = false;
    }

    public boolean shouldRead (){
        return iClientShouldListen;
    }
    /**
     *  returns arguments passed
     * @param args              list of arguments received
     * @return                  list of args:  String []
     * @apiNote                 The required args are first. They are followed by the external debugging args
     * @throws IOException      raised when not all the required args are passed
     */
    private String[] parseArgs (String[] args) throws IOException{
        String[] argVals =  new String[ requiredArgs.length +  externalDebuggerArgs.length];
        boolean foundAllRequiredArgs =  true;
        // parsing required args
        for (int i = 0; i < requiredArgs.length; i++) {
            String argVal = "";
            for (String arg : args) {
                if (arg.toLowerCase().trim().startsWith(requiredArgs[i])) {
                    argVal= arg.toLowerCase().trim().replace(requiredArgs[i], "").replace("=", "").trim();
                    break;
                }
            }
            if (argVal.length() > 0) {
                argVals[i] = argVal;
            } else {
                foundAllRequiredArgs = false;
                break;
            }
        }
        if (! foundAllRequiredArgs) {
            throw new IOException("Error: Incomplete configuration.\nExpected the following arguments: mas2j, environment & scenario");
        }
        for (int i =0; i < externalDebuggerArgs.length; i++) {
            String argVal = "";
            for (String arg : args) {
                if (arg.toLowerCase().trim().startsWith(externalDebuggerArgs[i])) {
                    argVal =  arg.toLowerCase().replace(externalDebuggerArgs[i], "").replace("=", "").trim();
                    break;
                }
            }
            if (argVal.length() > 0) {
                argVals[ requiredArgs.length + i] = argVal;
            } else {
               break;
            }
        }
        return argVals;
    }

    /**
     * returns the name and configuration of all agents in the mas2j file
     * @param path    path to mas2j file
     * @return        an array containing the name and configuration of all agents
     * @apiNote       structure: [agent_1_name, agent_1_capacity, agent_1_speed, agent_1_resurce_type, agent_2_name, ... agent_n_resource_type]
     * @throws FileNotFoundException
     * @throws jason.mas2j.parser.ParseException
     * @throws NumberFormatException
     */
    private String[] parseMas2J (String path) throws FileNotFoundException, jason.mas2j.parser.ParseException, NumberFormatException{
        final String[] configs = new String[] {"capacity", "scan_range", "speed", "resource_type"};
        final jason.mas2j.parser.mas2j mas2jParser = new jason.mas2j.parser.mas2j(new FileInputStream(path));
        final MAS2JProject project = mas2jParser.mas();

        int trueAgentCount = 0;

        for (int i = 0; i < project.getAgents().size(); i++) {
            trueAgentCount += project.getAgents().get(i).getNbInstances();
        }

        System.out.println("  - found " +  trueAgentCount + " agents");
        String[] informationAboutAgents = new String[5 * trueAgentCount];

        for (int agentIndex=0; agentIndex < project.getAgents().size(); agentIndex++) {

            final AgentParameters params = project.getAgents().get(agentIndex);
            final int agentInstances = params.getNbInstances();

            for (int instanceNumber=0; instanceNumber < agentInstances; instanceNumber++) {

                informationAboutAgents[(agentIndex + instanceNumber) * 5] =  agentInstances > 1 ? params.getAgName() + (instanceNumber + 1) : params.getAgName();

                for (int configCounter=0; configCounter < configs.length; configCounter++) {
                    informationAboutAgents[ (5 * (agentIndex + instanceNumber)) + (configCounter + 1)] = params.getOption(configs[configCounter]);
                }
            }
        }

        return informationAboutAgents;
    }


    /**
     * Executes agents actions
     * @param ag        agent name
     * @param action    action being performed
     * @return
     */
    @Override
    public boolean executeAction(String ag, Structure action) {

        if (isStartOfCycle && (model != null)) {
            model.onReasoningCycleStarted();
            isStartOfCycle = false;
        }

        int agent =  model.getAgentId(ag);
        try {
            maskFeedback[agent] = true;

            if (action.getFunctor().equals(RoverWorldModel.Action.COLLECT.getName())) {
                feedback[agent] =  model.collectResource(agent);
                maskFeedback[agent] = false;
            } else if (action.getFunctor().equals(RoverWorldModel.Action.DEPOSIT.getName())) {
                feedback[agent] = model.depositResource(agent);
                maskFeedback[agent] = false;
            } else if (action.getFunctor().equals(RoverWorldModel.Action.MOVE.getName())) {

                try{
                    int nParams =  action.getTerms().size();
                    int[] extraInfo = new int[nParams -3];
                    final int x = (int) ((NumberTerm) action.getTerm(0)).solve();
                    final int y = (int) ((NumberTerm) action.getTerm(1)).solve();
                    final int s = (int) ((NumberTerm) action.getTerm(2)).solve();
                    if (nParams > 3) {
                        for (int i = 0; i < extraInfo.length; i++) {
                            extraInfo[i] = (int) ((NumberTerm) action.getTerm(3 + i)).solve();
                        }
                    }
                    feedback[agent] =  model.move(agent, x, y, s, extraInfo);

                    // final should have one....
                }catch (final Exception ex) {

                    feedback[agent] = new ActionFeedback(RoverWorldModel.Action.MOVE.getName(), RoverWorldModel.ActionStatusCode.INVALID_ACTION, new int[]{RoverWorldModel.InvalidActionStatuscode.INVALID_PARAM.getCode()});
                }


            } else if (action.getFunctor().equals(RoverWorldModel.Action.SCAN.getName())) {
                try {
                    final int range = (int) ((NumberTerm) action.getTerm(0)).solve();
                    feedback[agent] = model.scan(agent, range);
                    maskFeedback[agent] = false;
                } catch (final Exception ex) {
                    feedback[agent] = new ActionFeedback(RoverWorldModel.Action.SCAN.getName(), RoverWorldModel.ActionStatusCode.INVALID_ACTION, new int[]{RoverWorldModel.InvalidActionStatuscode.INVALID_PARAM.getCode()});
                }
            } else {
                feedback[agent] = new ActionFeedback(action.getFunctor(), RoverWorldModel.ActionStatusCode.INVALID_ACTION, new int[]{RoverWorldModel.InvalidActionStatuscode.UNPERMITTED.getCode()});
                maskFeedback[agent] = false;
            }

        } catch (NullPointerException npe) {
            if (model == null) {
                System.out.println("Scenario  has ended.");
                return true;
            }
            return false;

        }

        boolean rs = (agent > -1) && (feedback[agent].getStatus() != RoverWorldModel.ActionStatusCode.INVALID_ACTION)  && (feedback[agent].getStatus() != RoverWorldModel.ActionStatusCode.MOVEMENT_OBSTRUCTED) ;

        return rs;
    }

    @Override
    public void unmaskFeedback (String agent) {
        int agId =  model.getAgentId(agent);
        try {
            maskFeedback[agId] = false;
        }catch (Exception ex){
            System.out.println("Could not unmask feedback for: " + agent);
        }
    }


    @Override
    public void clearAgentFeedback (String agent) {
        int agId =  model.getAgentId(agent);
        try {
            feedback[agId] = null;
        }catch (Exception ex){
            System.out.println("Could not clear feedback for: " + agent);
        }
    }




    /**
     * Updates the percept of all agents
     */
    @Override
    protected void updateAgsPercept() {
        //System.out.println("************ NEW RF ****************");
            if (model == null) { return; }

            for (int i = 0; i < model.getNbAgs(); i++) {
                updateAgPercept(i);
            }

            model.onReasoningCycleEnded();



            try{
                long sleep = 500;
                if (sleep > 0) {
                    Thread.sleep(sleep);
                }
            } catch (InterruptedException ie) {
                System.out.println("Process Interrupted... dont worry");
            }
            isStartOfCycle = true;
            model.onReasoningCycleStarted();
    }

    public void onEndConditionDetected(String reason) {
        System.out.println("Scenario ended with reason: " + reason);
        System.out.println("********************************************** DISCARD ALL INFORMATION BELOW THIS LINE ********************************************** \n\n\n\n\n");
        endSimulation(reason);
    }

    /**
     * Updates the percept of an agent
     * @param agent Agent ID
     */
    private void updateAgPercept(int agent) {
        if (model == null) { return; }
        final ActionFeedback agentFeedback = feedback[agent];


        final String agentName = model.getAgName(agent);

        clearPercepts(agentName);

        if ( (agentFeedback != null) && ! maskFeedback[agent]) {
            if (agentFeedback.getStatus() != RoverWorldModel.ActionStatusCode.RESOURCE_FOUND) {
                //System.out.println("UP per of: " + agentName + "   --> " + agentFeedback.getStatus().getLiteralString() );
                addPercept(agentName, Literal.parseLiteral(agentFeedback.getStatus().getLiteralString() + agentFeedback.getFeedbackParams()));
            } else {
               String[] resourcesFound = agentFeedback.getFeedbackParams(4);
               for (String resourceParam :  resourcesFound) {
                 //  System.out.println("UP per of: " + agentName + "   --> " + agentFeedback.getStatus().getLiteralString() );
                   addPercept(agentName, Literal.parseLiteral(agentFeedback.getStatus().getLiteralString() + resourceParam));
               }
            }
        }
    }

    /**
     *  Informs all agents that the simulation has ended and destroys the model
     */
    private void endSimulation (String reason) {
        model = null;
        RoverWorldModel.destroy();
        addPercept(Literal.parseLiteral("scenario_ended"));
        informAgsEnvironmentChanged();
    }

    @Override
    protected boolean isComposite (String agName , Structure action) {
        if (action.getFunctor().equals("move")) {
            try{
                final int x = (int) ((NumberTerm) action.getTerm(0)).solve();
                final int y = (int) ((NumberTerm) action.getTerm(1)).solve();
                final int speed = (int) ((NumberTerm) action.getTerm(2)).solve();
                return model.splitJourneyBasedOnTimeSteps(model.getAgentId(agName), x, y, speed).size() > 1;

                        //model.requiresMoreThanOneStep(model.getAgentId(agName), x, y, speed);
            } catch (Exception ex) {
                // lazy..... should probably do something here.
            }
        }
        return false;
    }

    /**
     * @see StepSynchedEnvironment#isComposite(String, Structure)
     * @param agName    agent name
     * @param action    action queued
     * @return
     */
    @Override
    protected ArrayList <Structure> getComposites (String agName, Structure action) {
        ArrayList<Structure> resp =  new ArrayList<Structure>();
        if (action.getFunctor().equals("move")) {
            try {
                final int x = (int) ((NumberTerm) action.getTerm(0)).solve();
                final int y = (int) ((NumberTerm) action.getTerm(1)).solve();
                final int speed = (int) ((NumberTerm) action.getTerm(2)).solve();

                ArrayList<Integer[]> wayPoints = model.splitJourneyBasedOnTimeSteps(
                        model.getAgentId(agName),
                        x, y, speed
                );

                for (Integer[] point : wayPoints) {
                    Structure newAction =  (Structure) action.clone();
                    newAction.setTerm(0, new NumberTermImpl(point[0]));
                    newAction.setTerm(1, new NumberTermImpl(point[1]));
                    newAction.setTerm(2, new NumberTermImpl(speed));
                    for (int passedVar = 2; passedVar < point.length; passedVar++) {
                        newAction.addTerm(new NumberTermImpl(point[passedVar]));
                    }
                    resp.add(newAction);
                }
            } catch (final Exception ex) {
                resp.add(action);
            }
        }
        return resp;
    }

    /**
     * @see jason.environment.TimeSteppedEnvironment
     */
    @Override
    protected void stepStarted(int step) {
        //do something here... if needed
    }

    /**
     * @see jason.environment.TimeSteppedEnvironment
     */
    @Override
    protected void stepFinished(int step, long time, boolean timeout) {
        // do something here..... if needed.
    }

    /* Internal action helper functions */

    /**
     * returnns the energy level of an agent
     * @param agent     Agent's name : String
     * @return          Agent's energy level : double
     */
    public static double getEnergy (String agent) {
        return model != null ? model.getEnergy(model.getAgentId(agent)) : 0.0;
    }

    /**
     * returns the scan range of an agent
     * @param agent    Agent's name : String
     * @return         Agent's scan range : int
     */
    public static int getScanRange (String agent) {
        return model != null ? model.getScanRange(model.getAgentId(agent)) : 0;
    }

    /**
     *  returns the capacity of an agent
     * @param agent     Agent's name    : String
     * @return          Agent's capacity : int
     */
    public static int getCapacity (String agent) {
        return model != null ? model.getCapacity(model.getAgentId(agent)) : 0;
    }

    /**
     * returns the max speed of an agent
     * @param agent     Agent's name    : String
     * @return          Agent's max speed : int
     */
    public static int getSpeed (String agent) {
        return model != null ? model.getSpeedLimit(model.getAgentId(agent)) : 0;
    }

    /**
     * returns the resource type an agent is allowed to carry
     * @param agent     Agent's name : String
     * @return          Agent's resouces config : int
     */
    public static String getResourceType (String agent) {
        return model != null ?  RoverWorldModel.GridItem.getLiteralString(model.getPreferredType(model.getAgentId(agent))) : "undefined";
    }

    /**
     * returns the map size
     * @return  map size as {width, height} or {0,0} if model has been destroyed/ not initialised
     */
    public static int[] getMapSize () {
        return model == null ? new int[]{ 0, 0} :  model.getSize();
    }

}
