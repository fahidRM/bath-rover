package rover;

/**
 *  Listener interface to indicate when a grid tile has been updated
 *  This is intended to help weed out unnecessarily refreshing the UI when nothing has changed or when the change is out of sight
 */
public interface GridChangeListener {
    void onGridTileItemChange (int xPos, int yPos, int[] items, int[] itemsCount);
    void onGridTileItemQuantityChange (int xPos, int yPos, int[] items, int[] itemsCount);
}
