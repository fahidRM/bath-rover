package rover;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;


public class Scenario {
    private final int DEFAULT_RESSOURCE_TYPE = -1;

    private final int id;
    private final int agCount;
    private final String energyCategory;
    private final int height;
    private final String obstacleDensity;
    /* no. of resource per pile */
    private final int resourceCount;
    /* no. of distinct resource piles */
    private final int resourceDist;
    private final int width;

    private final String version;
    private final double energyCostPerReasoningCycle;
    private final boolean launchOnFullScreen;

    private int[] agXPos;
    private int[] agYPos;
    private int[] baseLocation;
    private int[] obXPos;
    private int[] obYPos;
    private int[] rsType;
    private int[] rsCount;
    private int[] rsXPos;
    private int[] rsYPos;


    /**
     * Returns the maximum number of Agents that can be used in the scenario.
     * @return  max. allowed agents
     */
    public int getAgCount() { return Collections.min(Arrays.asList(agXPos.length, agYPos.length)); }

    /**
     * Returns the location of the Base in the scenario
     * @return  Base location (x-position, y-position)
     */
    public int[] getBaseLocation () { return baseLocation; }

    /**
     * Returns the energy cost of a resoning cycle
     * @return  amount of energy deducted per agent per reasoning cycle
     */
    public double getEnergyCostPerReasoningCycle() { return energyCostPerReasoningCycle / 2.5; }

    /**
     * Returns the scenario's ID
     * @return  scenario ID
     */
    public int getId () { return id; }

    /**
     * Returns the amount of energy each agent has the start of the simulation
     * @return  Initial energy of all agents
     */
    public double getInitialEnergy () {    return calculateInitialEnergy(); }

    /**
     * Retuns the grid height
     * @return  Grid height
     */
    public int getHeight () { return height; }

    /**
     * Returns the resource distribution
     * @return no. of distinct resource piles
     */
    public int getResourceDist () { return resourceDist != 0 ? resourceDist : Collections.min(Arrays.asList(rsXPos.length, rsYPos.length,rsCount.length, rsType.length)); }


    public int getObstacleCount () { return calculateObstacleCount(); }
    /**
     * Returns the grid width
     * @return  Grid idth
     */
    public int getWidth () { return width; }

    /**
     * Returns the scenario definition version
     * @return  Scenario file version string
     */
    public String getVersion () { return version; }



    /**
     *  Returns the position of an agent on the grid
     * @param agIndex   Agent's index  (positive integer)
     * @return          Agent's location on grid as array: [X position,Y position]
     * @apiNote         See {@link Scenario#getAgCount}. agIndex ranges from 0 to the value of {@link Scenario#getAgCount}.
     */
    public int[] getAgPos (int agIndex) {
        return  (agIndex >= 0) && (agIndex < agCount) && (agIndex < agXPos.length) && (agIndex < agYPos.length) ? new int[] { agXPos[agIndex], agYPos[agIndex]} : new int[] {};
    }

    /**
     * Returns the postition of an obstacle in the gris
     * @param obIndex Obstacles's index (positive integer)
     * @return        Obstacles location on grid as array: [X position, Y position]
     */
    public int[] getObstaclePos (int obIndex) {
        return  (obIndex >= 0) &&  (obIndex < obXPos.length) && (obIndex < obYPos.length) ? new int[] { obXPos[obIndex], obYPos[obIndex]} : new int[] {};
    }

    /**
     * Returns the location, type and count of a resource deposit in the grid
     * @param rsIndex   resource index (positive integer)
     * @return  resource information as array: [x position y position, type, count]
     * @apiNote see {@link Scenario#getResourceDist}. rsIndex ranges from 0 to the value of  {@link Scenario#getResourceDist}.
     */
    public int[] getResourceInfo (int rsIndex) {
        return (rsIndex >= 0) && (rsIndex < getResourceDist()) && (rsIndex < rsXPos.length ) && (rsIndex < rsYPos.length ) && (rsIndex < rsType.length ) && (rsIndex < rsCount.length ) ? new int[] { rsXPos[rsIndex], rsYPos[rsIndex], rsType[rsIndex], rsCount[rsIndex]} : new int[] {};
    }

    /**
     * Returns the total resource count
     * @return  total resource count
     */
    public int getTotalRSCount () {
        // If the location has been placed manually (complex map specification)
        if ((resourceCount * resourceDist) == 0) {
            int lengthToConsider = Collections.min(Arrays.asList(rsXPos.length, rsYPos.length,rsCount.length, rsType.length));
            int sum = 0;
            for (int i = 0;  (i < lengthToConsider) && (i < rsCount.length);  i++) {
                sum += rsCount[i];
            }
            return sum;
        }
        // If a simple scenario definition was used to set up this object
        return resourceCount * resourceDist;
    }


    public boolean shouldLaunchOnFullscreen () {
        return launchOnFullScreen;
    }

    /**
     * Constructor: Random generated map
     * @param i     scenario id
     * @param ac    number of agents
     * @param e     energy category
     * @param h     grid height
     * @param rc    resource pile count
     * @param rd    resource distribution
     * @param rts   resource types
     * @param w     grid width
     */
    public Scenario (int i, int ac, String e, int h, int rc, int rd, int[] rts, int w, String od, String v, double ec, boolean fullscreen) {
        id =  i;
        agCount = ac;
        energyCategory = e;
        height = h;
        resourceCount = rc;
        resourceDist = rd;
        width = w;
        version = v;
        energyCostPerReasoningCycle = ec;
        obstacleDensity = od;
        launchOnFullScreen = fullscreen;

        // sets up internal arrays only if rc & rd are specified --> if scenario was built from a simple map specification
        if  ((rc +  rd) > 0 ){
            // use the default resource type if resource types were not specified
            if (rts.length == 0) { rts = new int[]{DEFAULT_RESSOURCE_TYPE}; }
            // randomly decide start position (base location)
            baseLocation =  new int[] {(new Random()).nextInt(width), (new Random()).nextInt(height)};
            // place all agents at the base
            agXPos = new int[agCount];
            agYPos = new int[agCount];
            for (int z = 0; z < agCount; z++) {
                agXPos[z] = baseLocation[0];
                agYPos[z] = baseLocation[1];
            }
            // randomly decide the location of the resources
            rsType = new int[rd];
            rsCount = new int[rd];
            rsXPos = new int[rd];
            rsYPos = new int[rd];

            double typeCheck = 0;

            for (int y = 0; y < rd; y++) {
                // provisional coordinates
                int provX =  baseLocation[0];
                int provY =  baseLocation[1];
                // ensure no resource is dropped at the base
                while ( (provX == baseLocation[0]) && (provY == baseLocation[1])) {
                    provX =  (new Random()).nextInt(width);
                    provY =  (new Random()).nextInt(height);
                }
                // ensures that there is a mix of resource type
                if (rts.length > 1) {
                    int provType = rts[(new Random()).nextInt(rts.length)];

                    if (y > 0) {
                        final double averageOfTypesPlaced =  typeCheck / (double) (y+1);
                        while ((double) provType == averageOfTypesPlaced) {
                            provType = rts[(new Random()).nextInt(rts.length)];
                        }
                    }


                    typeCheck +=  provType;
                    rsType[y] = provType;
                } else {
                    rsType[y] =  rts[0];
                }
                rsXPos[y] = provX;
                rsYPos[y] = provY;
                rsCount[y] = rc;
            }

            obXPos =  new int[calculateObstacleCount()];
            obYPos =  new int[obXPos.length];

            for (int z = 0; z < obXPos.length; z++) {
                int provX =  baseLocation[0];
                int provY =  baseLocation[1];
                // ensure no resource is dropped at the base
                while (isOnResourceOrBase(provX, provY)) {
                    provX =  (new Random()).nextInt(width);
                    provY =  (new Random()).nextInt(height);
                }
                obXPos[z] =  provX;
                obYPos[z] =  provY;
            }

            // TODO: check for enclosures and fix
        }



    }

    /**
     *
     * @param i
     * @param e
     * @param h
     * @param w
     * @param base
     * @param agx
     * @param agy
     * @param obx
     * @param oby
     * @param rsc
     * @param rst
     * @param rsx
     * @param rsy
     * @param v
     * @param ec
     * @param fullscreen
     */
    public Scenario  (int i, String e, int h, int w, int[] base,  int[] agx, int[] agy, int[] obx, int[] oby, int[] rsc,  int[] rst, int[] rsx, int[] rsy, String v, double ec, boolean fullscreen ) {
        this(i, agx.length, e, h,  0, 0, new int[]{}, w, null, v, ec, fullscreen);
        agXPos =  agx;
        agYPos = agy;
        baseLocation = base;
        obXPos = obx;
        obYPos = oby;
        rsCount = rsc;
        rsType = rst;
        rsXPos = rsx;
        rsYPos = rsy;
    }

    /**
     *
     * @param i
     * @param fullscreen
     * @param e
     * @param od
     * @param rts
     * @param ac
     * @param ec
     * @param v
     */
    public Scenario (int i, boolean fullscreen, String e, String od, int[] rts, int ac, double ec,String v){

        id = i;
         agCount = ac;
         energyCategory = e;
         version = v;
         rsType =  rts;
         energyCostPerReasoningCycle = ec;
         obstacleDensity = od;
         launchOnFullScreen = fullscreen;
         int upperLimit =  52;
         int lowerlimit =  45;
         int selectedHeight = lowerlimit + ((new Random()).nextInt(upperLimit - lowerlimit));
         height =  selectedHeight;
         width  = selectedHeight;
         int maxRSCount =  3;
         int maxRSDist =  7;
         resourceCount =  1 + ((new Random()).nextInt(maxRSCount));
         resourceDist = 5  +  ((new Random()).nextInt(maxRSDist));

         if  ((resourceCount +  resourceDist) > 0 ){
             // use the default resource type if resource types were not specified
             if (rts.length == 0) { rts = new int[]{DEFAULT_RESSOURCE_TYPE}; }
             // randomly decide start position (base location)
             baseLocation =  new int[] {(new Random()).nextInt(width), (new Random()).nextInt(height)};
             // place all agents at the base
             agXPos = new int[agCount];
             agYPos = new int[agCount];
             for (int z = 0; z < agCount; z++) {
                 agXPos[z] = baseLocation[0];
                 agYPos[z] = baseLocation[1];
             }
             // randomly decide the location of the resources
             rsType = new int[resourceDist];
             rsCount = new int[resourceDist];
             rsXPos = new int[resourceDist];
             rsYPos = new int[resourceDist];

             double typeCheck = 0.0;

             for (int y = 0; y < resourceDist; y++) {
                 // provisional coordinates
                 int provX =  baseLocation[0];
                 int provY =  baseLocation[1];
                 // ensure no resource is dropped at the base
                 while ( (provX == baseLocation[0]) && (provY == baseLocation[1])) {
                     provX =  (new Random()).nextInt(width);
                     provY =  (new Random()).nextInt(height);
                 }
                 // ensures that there is a mix of resource type
                 if (rts.length > 1) {
                     int provType = rts[(new Random()).nextInt(rts.length)];
                     if (y > 0) {
                         while (  ((double) provType) == ( (typeCheck + ((double) provType) / (y + 1) )) ){
                             provType = rts[(new Random()).nextInt(rts.length)];
                         }
                     }
                     rsType[y] = provType;
                 } else {
                     rsType[y] =  rts[0];
                 }
                 rsXPos[y] = provX;
                 rsYPos[y] = provY;
                 rsCount[y] = resourceCount;
             }

             obXPos =  new int[calculateObstacleCount()];
             obYPos =  new int[obXPos.length];

             for (int z = 0; z < obXPos.length; z++) {
                 int provX =  baseLocation[0];
                 int provY =  baseLocation[1];
                 // ensure no resource is dropped at the base
                 while (isOnResourceOrBase(provX, provY)) {
                     provX =  (new Random()).nextInt(width);
                     provY =  (new Random()).nextInt(height);
                 }
                 obXPos[z] =  provX;
                 obYPos[z] =  provY;
             }

             // TODO: check for enclosures and fix
         }


     }




    /**
     * Calculates the amount of energy to give each agent at start based on the given energy category and the generated or specified scenario
     * @return  initial energy
     */
    private double calculateInitialEnergy () {
        double baseEnergy = 0.0;
        double resourceToAgentRatio =  ((double)getTotalRSCount()) / ((double)getAgCount());
        double squareArea = height * width;

        double sizeScaleUpFactor = (squareArea /  this.agCount);
        double obstacleCompensation = squareArea / getObstacleCount();


        switch(this.energyCategory.toLowerCase()) {
            case "very_low":
                baseEnergy = 1000;
                return baseEnergy;

            case "low":
                baseEnergy = 1500;
                return baseEnergy + sizeScaleUpFactor + (1.75 * (obstacleCompensation)) +  (0.65 * resourceToAgentRatio);

            case "medium":
                baseEnergy = 2750  + sizeScaleUpFactor + (1.22 * (obstacleCompensation)) +  (0.5 * resourceToAgentRatio);;
                return baseEnergy;

            case "high":
                baseEnergy = 3750;
                return baseEnergy;

            case "very_high":
                baseEnergy = 4200;
                return baseEnergy;

            case "extremely_high":
                baseEnergy = 5000;
                return baseEnergy;

            default:
                // useful in cases where the energy has been specified manually
                // prevents the need to adjust the definition of an energy category
                // should the need to make changes arise.
                try {
                    return Double.parseDouble(this.energyCategory);
                }catch (Exception ex) {
                }
                return 0.0;
        }
    }


    /**
     * Calculates the amount of obstacles to place on a map
     * @return  obstacles that will be present on map
     */
    private int calculateObstacleCount () {

        if ((obXPos != null) && (obXPos.length > 0)) { return obXPos.length; }

        int totalGridSpace =  this.width * this.height;
        switch (this.obstacleDensity.toLowerCase()) {
            case "none":
                return 0;
            case "very_low":
                return 3 * totalGridSpace / 100;
            case "low":
                return 5 * totalGridSpace / 100;
            case "medium":
                return 8 * totalGridSpace / 100;
            case "high":
                return totalGridSpace /10;
            case "very_high":
                return 15 * totalGridSpace / 100;
            default:
                // Obstacle density can also be an integer value in case we want to specify
                try {
                    return Integer.parseInt(this.obstacleDensity);
                }catch (Exception ex) {
                }
                return 0;
        }
    }


    private int[] detectEnclosuresAndSuggestRemovals () {
        return new int[] {};
    }

    private boolean isOnResourceOrBase (int provX, int provY) {
        if ((provX == baseLocation[0]) && (provY == baseLocation[1])) { return true; }
        for (int i = 0; i < rsXPos.length; i++) {
            if ((rsXPos[i] == provX) && (rsYPos[i] == provY)) {
                return true;
            }
        }

        return false;
    }
}
