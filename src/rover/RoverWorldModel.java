package rover;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class RoverWorldModel implements RoverWorldViewListener{

    private static final String version = "version 2.2.2a";

    // actions
    public enum Action {
        MOVE ("move", "{DIST} * {SPEED}"),
        DEPOSIT ("deposit", ""),
        COLLECT ("collect", ""),
        SCAN ("scan", "");

        private String name;
        // private String costEqn;

        public String getName() {
            return name;
        }

        Action (String actionName, String costFormula) {
            name = actionName;
            // costEqn =  costFormula;
        }
    }

    // Status code for actions
    public enum ActionStatusCode {
        INSUFFICIENT_ENERGY ("insufficient_energy", 1),
        INVALID_ACTION("invalid_action", 0),
        ACTION_COMPLETED("action_completed", 0),
        RESOURCE_FOUND("resource_found", 10000),
        RESOURCE_NOT_FOUND("resource_not_found", 0),
        MOVEMENT_OBSTRUCTED("movement_obstructed", 4);


        private String literalString;
        private int paramCount;

        public String getLiteralString() { return literalString; }
        // public int getParamCount() { return paramCount; }

        ActionStatusCode (String literalStr, int maxParamCount) {
            literalString =  literalStr;
            paramCount = maxParamCount;
        }
    }

    // Status code for invalid actions
    public enum InvalidActionStatuscode {

        INVALID_PARAM("invalid_param", 0),
        UNMET_REQUIREMENT("unmet_requirement", 1),
        UNPERMITTED ("unpermitted", 2);

        private String literalString;
        private int code;

        public String getLiteralString() { return literalString; }
        public int getCode() { return code; }

        public static String getLiteralString (int val) {
            switch(val) {
                case 0:
                    return INVALID_PARAM.literalString;
                case 1:
                    return UNMET_REQUIREMENT.literalString;
                case 2:
                default:
                    return UNPERMITTED.literalString;
            }
        }

        InvalidActionStatuscode (String literalStr, int codeVal) {
            literalString =  literalStr;
            code =  codeVal;
        }

    }

    // Items that can be placed on the Grid
    public enum GridItem {
        AGENT ("Agent" , 32, new int[]{}, 10),
        BASE ("Base", 8, new int[]{}, 2),
        DIAMOND ("Diamond", 4, new int[]{}, 1),
        GOLD("Gold", 2, new int[]{}, 1),
        SCAN_REGION("Scan Region", 16, new int[] {155,229,155, 35}, 2),
        OBSTACLE ("Obstacle", 64, new int[]{}, 1),
        UNDEFINED ("Null", -1, new int[]{}, 0);


        public static String getLiteralString (int val) {
            switch(val) {
                case 32:
                    return AGENT.name;
                case 8:
                    return BASE.name;
                case 4:
                    return DIAMOND.name;
                case 2:
                    return GOLD.name;
                case 16:
                    return SCAN_REGION.name;
                case 64:
                    return OBSTACLE.name;
                default:
                    return UNDEFINED.name;
            }
        }

        private int code;
        // private int[] colour;
        // private int weight;
        private String name;

        public int getCode () {
            return code;
        }
        // public int[] getColour() { return colour; }
        // public int getWeight () { return weight; }
        public String getName() {return name;}


        /**
         *
         * @param gridCode      Item code. see {@link Grid#addItem}
         * @param colourCode    Item colour and opacity when rendered
         * @param itemWeight    Item placement priority
         * @apiNote weight ideally should be between 0 and 10. 10 is analogous to always on top while 0 means always at the bottom
         */
        GridItem (String itemName, int gridCode, int[] colourCode, int itemWeight) {
            name = itemName;
            code = gridCode;
            //colour =  colourCode;
            //weight =  itemWeight;
        }

    }

    /* Items that can be found by scanning the map */
    private static final int[] DETECTABLE_ITEMS = new int[] {
            GridItem.DIAMOND.code,
            GridItem.GOLD.code,
            GridItem.OBSTACLE.code
    };

    private static RoverWorldModel model;
    private static RoverWorldModelEndListener endListener;
    private Grid grid;
    private RoverWorldView view;

    private final double energyCostPerReasoningCycle;
    private boolean persistScanRegion = false;

    /* Agents' config */
    private String[] agNames;
    private int[] agCapacity;
    private int[] agRange;
    private int[] agSpeedLimit;
    private int[] agPrefResource;

    /* Agents' state */
    private double[] agEnergy;
    private double[] agRequiredEnergy;
    private int[] agResourceHeld;
    private int[] agResHeldType;
    private int[] agXPos;
    private int[] agYPos;

    private int[] obXPos;
    private int[] obYPos;

    /* Resource tracking */
    private int[] availableRes;
    private int[] collectedRes;
    private int totalResources;

    private String terminationReason =  "UNKNOWN";

    private int[] scanLocationX;
    private int[] scanLocationY;

    private boolean[] actedInCurrentCycle;


    /**
     * Constructor
     * @param h     grid height
     * @param w     grid width
     * @param nAg   no. or agents
     * @param eprc  energy cost per reasoning cycle.... TODO: refactor..
     * @return
     */
    synchronized private static RoverWorldModel create (int h, int w, int nAg, double eprc) {
        if (model == null) {
            model =  new RoverWorldModel(h, w, nAg, eprc);
        }
        return model;
    }

    /**
     * Builds a model from a configuration
     * @param listener              End of scenario listener for the model being created
     * @param scenario              Scenario being read
     * @param agNameList            Names of all Agents
     * @param agCapacityList        Max capacity for all agents
     * @param agSpeedList           Max speed for all agents
     * @param agRangeList           Max scan range for all agents
     * @param agPrefResList         Pref resource for all agents
     *                              TODO: bundle the agents' info into scenario.....
     * @return                      Model for the specified scenario
     */
    public static RoverWorldModel buildModel (@NotNull RoverWorldModelEndListener listener, @NotNull Scenario scenario, @NotNull String[] agNameList, int[] agCapacityList, int[] agSpeedList, int[] agRangeList, String[] agPrefResList) {

        RoverWorldModel.model = null;
        RoverWorldModel.endListener = listener;

        // used agNameList to set up the model because the max allowed agents in the scenario might be greater than the actual
        // number of agents being used to solve the scenario.
        final RoverWorldModel model = RoverWorldModel.create(scenario.getHeight(), scenario.getWidth(), agNameList.length, scenario.getEnergyCostPerReasoningCycle());
        // store agents' configuration
        model.availableRes =  new int[] {0, 0, 0, 0, 0, 0};
        model.collectedRes =  new int[] {0, 0, 0, 0, 0, 0};
        model.agCapacity = agCapacityList;
        model.agNames =  agNameList;
        model.agPrefResource =  new int[agPrefResList.length];

        model.scanLocationX = new int[agPrefResList.length];
        model.scanLocationY = new int[agPrefResList.length];
        model.actedInCurrentCycle = new boolean[agPrefResList.length];
        for (int iiii = 0; iiii < agPrefResList.length; iiii++){
            model.actedInCurrentCycle[iiii] =  false;
        }


        for (int i = 0; i < agPrefResList.length; i++) {
            if (agPrefResList[i] == null){
                model.agPrefResource[i] = GridItem.UNDEFINED.code;
            } else if (agPrefResList[i].equals(GridItem.GOLD.getName())) {
                model.agPrefResource[i] =  GridItem.GOLD.code;
            } else if (agPrefResList[i].equals(GridItem.DIAMOND.getName())) {
                model.agPrefResource[i] = GridItem.DIAMOND.code;
            } else {
                model.agPrefResource[i] = GridItem.UNDEFINED.code;
            }
        }

        model.agRange = agRangeList;
        model.agSpeedLimit = agSpeedList;
        // place agents on the grid and keep track of their energy
        final double initialEnergy = scenario.getInitialEnergy();
        for (int i = 0; i < scenario.getAgCount() && i < agNameList.length; i++) {
            int[] agPos =  scenario.getAgPos(i);
            model.agXPos[i] = agPos[0];
            model.agYPos[i] = agPos[1];
            model.agEnergy[i] = initialEnergy;
            model.grid.addItem(GridItem.AGENT.code, agPos[0], agPos[1], 1);
        }
        // place base on the grid
        model.grid.addItem(GridItem.BASE.code, scenario.getBaseLocation()[0], scenario.getBaseLocation()[1], 1);
        // place resources on the grid
        for (int i = 0; i < scenario.getResourceDist(); i++) {
            int[] resourceInfo =  scenario.getResourceInfo(i);
            //3 => qty
            model.availableRes[resourceInfo[2]] =  model.availableRes[resourceInfo[2]] + resourceInfo[3];
            model.grid.addItem(resourceInfo[2], resourceInfo[0], resourceInfo[1], resourceInfo[3]);
        }

        int obCount =  scenario.getObstacleCount();
        model.obXPos = new int[obCount];
        model.obYPos =  new int[obCount];

        for (int i = 0; i  < model.obXPos.length; i++) {
            int[] vals =  scenario.getObstaclePos(i);
            model.obXPos[i] =  vals[0];
            model.obYPos[i] =  vals[1];
            model.grid.addItem(GridItem.OBSTACLE.code, vals[0], vals[1], 1);
        }


        model.totalResources = scenario.getTotalRSCount();
        model.view = new RoverWorldView(model.grid, model, scenario.getId(), version, scenario.shouldLaunchOnFullscreen());
        return model;
    }

    /**
     * Constructor
     * @param gridWidth         grid width
     * @param gridHeight        grid height
     * @param agentCount        agent count
     * @param energyCostPerRC   energy per res cycle -- billed when idle
     */
    private RoverWorldModel (int gridWidth, int gridHeight, int agentCount, double energyCostPerRC) {
        grid = new Grid (gridWidth, gridHeight);
        agSpeedLimit =  new int[agentCount];
        agCapacity =  new int[agentCount];
        agRange = new int[agentCount];
        agResourceHeld = new int[agentCount];
        agResHeldType = new int[agentCount];
        agXPos = new int[agentCount];
        agYPos = new int[agentCount];
        agPrefResource = new int[agentCount];
        agEnergy = new double[agentCount];
        agNames =  new String[agentCount];
        agRequiredEnergy = new double[agentCount];
        energyCostPerReasoningCycle = energyCostPerRC;
    }

    /**
     * returns the ID if an agent
     * @param agentName    Agent name
     * @return             Agent id { 0 and above if found and -1 if agent not found}
     */
    int getAgentId (String agentName) {
        for (int i = 0; i < agNames.length; i++) {
            if (agNames[i].equals(agentName)) { return i; }
        }
        return -1;
    }

    /**
     * gets an agent's name given its ID
     * @param ag    agent ID
     * @return      agent name where ID is valid otherwise null
     */
    String getAgName (int ag) {
        return isValidAgent(ag) ? agNames[ag] : null;
    }

    @Contract(pure = true)
    private boolean isValidAgent (int agent) {
        return (agent > -1) && (agent < agNames.length);
    }

    /**
     * gets an agent's current energy
     * @param agent agent ID
     * @return      agent's remaining energy. returns 0 where agent not found
     */
    double getEnergy (int agent) {
        return (isValidAgent(agent)) ? agEnergy[agent] : 0.0;
    }

    /**
     *
     * @param agent
     * @return          agent's max carry capacity. Returns 0 where agent ot found
     */
    int getCapacity (int agent) {
        return (isValidAgent(agent)) ? agCapacity[agent] : 0;
    }

    /**
     *
     * @param agent
     * @return
     */
    int getScanRange (int agent) {
        return (isValidAgent(agent)) ? agRange[agent] : 0;
    }

    /**
     *
     * @return
     */
    int[] getSize(){
        return  grid == null ? new int[]{} : new int[] {grid.getWidth(), grid.getHeight()};
    }

    /**
     *
     * @param agent
     * @return
     */
    int getSpeedLimit (int agent) {
        return (isValidAgent(agent)) ? agSpeedLimit[agent] : 0;
    }

    /**
     *
     * @param agent
     * @return
     */
    int getPreferredType (int agent) {
        return (isValidAgent(agent)) ? agPrefResource[agent] : 0;
    }

    /**
     *
     * @return
     */
    int getNbAgs () {
        return agNames.length;
    }

    /**
     *
     * @param agent                 agent id
     * @param xDisplacement         x-axis distance to travel
     * @param yDisplacement         y-axis distance to travel
     * @param speed                 speed to travel at
     * @param additionalInfo        additional information for the execution
     * @apiNote                     aditionalInfo....
     * @return                      action execution feedback
     */
    ActionFeedback move (int agent, int xDisplacement, int yDisplacement, int speed, int[] additionalInfo) {
        actedInCurrentCycle[agent] =  true;

        if (speed <= 0) {
            return new ActionFeedback(Action.MOVE.name, ActionStatusCode.INVALID_ACTION, new int[]{InvalidActionStatuscode.INVALID_PARAM.getCode()});
        }
        else if (speed > agSpeedLimit[agent]) {
            return new ActionFeedback(Action.MOVE.name, ActionStatusCode.INVALID_ACTION, new int[]{InvalidActionStatuscode.UNMET_REQUIREMENT.getCode()});
        }
        else {
           // Ideally all movements going through here will be for one step
            Integer[] unitSteps = hasObstacleOnPath(agent, xDisplacement, yDisplacement);

            int[] endPoint = grid.getNewLocationWithWrapEnabled(agXPos[agent], agYPos[agent], xDisplacement, yDisplacement);
            boolean brokeOut =  false;
            int skipX = 0;
            int skipY = 0;

            if (unitSteps.length == 1) {
                brokeOut =  true;
                endPoint = new int[]{ agXPos[agent], agYPos[agent] };
            } else if (unitSteps.length > 1) {
                brokeOut =  true;
                skipX =  unitSteps[0];
                skipY =  unitSteps[1];
                endPoint =  grid.getNewLocationWithWrapEnabled(agXPos[agent], agYPos[agent], unitSteps[0], unitSteps[1]);
            } else {
                brokeOut = false;
            }

            if (hasSufficientEnergy(agent, Action.MOVE, new int[] { /*endPoint[0], endPoint[1]*/ xDisplacement, yDisplacement, speed})) {

                grid.reduceItem(GridItem.AGENT.code, agXPos[agent], agYPos[agent], 1);

                int xTravelled = 0;
                int yTravelled = 0;
                int xLeft = 0;
                int yLeft =  0;


                if (brokeOut) {
                    if (additionalInfo.length >= 8) {
                        xTravelled = agXPos[agent] - additionalInfo[4];
                        yTravelled = agYPos[agent] - additionalInfo[5];
                        xLeft = additionalInfo[6] - xTravelled;
                        yLeft = additionalInfo[7] - yTravelled;
                    } else {
                        xTravelled = skipX;
                        yTravelled = skipY;
                        xLeft =  xDisplacement -  xTravelled;
                        yLeft =  yDisplacement - yTravelled;
                    }
                }

                // make the move
                agXPos[agent] =  endPoint[0];
                agYPos[agent] = endPoint[1];
                grid.addItem(GridItem.AGENT.code, endPoint[0], endPoint[1], 1);
                deductEnergy(agent);

                if (brokeOut) {
                    // action obstructed
                    int[] feedbackArray = new int[] {xTravelled, yTravelled, xLeft, yLeft};
                    return new ActionFeedback(Action.MOVE.name, ActionStatusCode.MOVEMENT_OBSTRUCTED, feedbackArray);
                } else {
                    // move completed
                    return new ActionFeedback(Action.MOVE.name, ActionStatusCode.ACTION_COMPLETED, additionalInfo);
                }
            }
             return new ActionFeedback(Action.MOVE.name, ActionStatusCode.INSUFFICIENT_ENERGY, new int[] { (int) agEnergy[agent]});
        }
    }


    /**
     * check if more than one steps are required for a move statement.
     * This is more efficient that checking the required steps and then performing a boolean operation
     * @param agent agent id
     * @param xDisp x-axis displacement
     * @param yDisp y-axis displacement
     * @param speed speed of travel
     * @return
     */
    boolean requiresMoreThanOneStep (int agent, int xDisp, int yDisp, int speed) {
        final int maxDistanceCoverableInATimestep = speed;
        //TODO: remove this .... (temp fix to handle a corner case;
        // distance for (1, 1) is sqrt(2) which is > 1 and ceils to 2 but takes only one step to execute....
        if ((Math.abs(xDisp) <= 1) && (Math.abs(yDisp) <= 1)) { return false; }
        return calculateDistance( xDisp, yDisp) > maxDistanceCoverableInATimestep;
    }

    /**
     * calculates the no. of steps required to perform a move
     * @param agent agent id
     * @param xDisp x-axis dislacement
     * @param yDisp y-axis displacement
     * @param speed speed of travel
     * @return
     */
    int requiredSteps (int agent, int xDisp, int yDisp, int speed) {
        if (requiresMoreThanOneStep(agent, xDisp, yDisp, speed)) {
            final int maxDistanceTravellable =  speed;
            final int distanceApart =  calculateDistance(xDisp, yDisp);
            return (int)Math.ceil(distanceApart/maxDistanceTravellable);
        }
        return 1;
    }


    /**
     * calculates the distance represented by teh given displacement
     *
     * @param xDisp displacement along the x-axis
     * @param yDisp displacement along the y-axis
     * @return
     */
    private int calculateDistance (int xDisp, int yDisp) {
        return (int) Math.ceil(Math.sqrt(xDisp * xDisp) + (yDisp * yDisp));
    }

    /**
     * Calculates the distance between two points
     * @param x1    x-coordinate of pt 1
     * @param y1    y-coordinate of pt 1
     * @param x2    x-coordinate of pt 2
     * @param y2    y-coordinate of pt 2
     * @return
     */
    private double calculateDistanceApart (int x1, int y1, int x2, int y2) {
        double xDiff =  x2 - x1;
        double yDiff =  y2 - y1;
        return Math.sqrt(((xDiff * xDiff) + (yDiff * yDiff))) ;
    }

    private double caculateTrueDistance(int xDisp, int yDisp) {
        return Math.sqrt(Math.abs(xDisp) +  Math.abs(yDisp));
    }

    private boolean checkPolarity (int a, int b) {
        return b-a > 0;
    }



    /**
     * splits a journey based into smaller journeys that can each be executed in one time step
     * @param agId      agent id
     * @param x         x-axis displacement
     * @param y         y-axis displacement
     * @param speed     speed of travel
     * @return          journey as steps
     */
    public ArrayList<Integer[]> splitJourneyBasedOnTimeSteps (int agId, int x, int y, int speed) {
        // add current location to the end of the array
        ArrayList<Integer[]> journeySteps = new ArrayList<>();

        if ( (x == 0) && (y==0)) { journeySteps.add(new Integer[]{ 0,0,0,0,0,0, agXPos[agId], agYPos[agId], x, y}); return journeySteps;}

        int maxDistPerUnitTime =  speed;
        int xDiff =  Math.abs(x);
        int yDiff = Math.abs(y);

        if ((x == 0) || (y == 0)) {	// Straight line motion
            boolean xVaries =  x != 0;
            int multiplier = (xVaries && (x < 0)) || (! xVaries && (y < 0)) ? -1 : 1;
            int distApart = xVaries ? xDiff : yDiff;
            int spillOff =  distApart % maxDistPerUnitTime;
            int stepsRequired =  (int)Math.ceil( (double)distApart /(double) maxDistPerUnitTime);
            int axisSum = 0;

            for (int iii = 0; iii < stepsRequired; iii++) {
                int iiJump = maxDistPerUnitTime;
                if (iii ==  stepsRequired -1) {
                    if (spillOff != 0) {
                        iiJump =  spillOff;
                    }
                }
                int[] currentLocation =  xVaries ? new int[] { iiJump * multiplier, 0} : new int[] {0, iiJump * multiplier};
                int[] distanceTravelled = xVaries ? new int[] { axisSum * multiplier, 0} : new  int[] { 0, axisSum * multiplier};
                int[] distanceLeft =  xVaries ? new int[] {(distApart - axisSum) * multiplier, 0} : new int[] {0, (distApart - axisSum) * multiplier};
               /* Waypoint point  =  new Waypoint (
                        currentLocation,
                        distanceTravelled,
                        distanceLeft
                );
                locations.add(point);*/

                journeySteps.add(new Integer[] {
                    currentLocation[0],
                        currentLocation[1],
                        distanceTravelled[0],
                        distanceTravelled[1],
                        distanceLeft[0],
                        distanceLeft[1],
                        agXPos[agId], agYPos[agId], x, y
                });


                axisSum += Math.abs(iiJump);
            }
        } else { // triangular motion

            int xMult =  x < 0 ? -1 : 1;
            int yMult =  y < 0 ? -1 : 1;

            double distApart = Math.ceil(Math.sqrt(((xDiff * xDiff) + (yDiff * yDiff))));
            if (maxDistPerUnitTime > distApart) { maxDistPerUnitTime = (int) Math.ceil(distApart); }
            int stepsRequired =  (int) Math.ceil(distApart / maxDistPerUnitTime);

            double theta =  Math.atan(((float) yDiff ) / ((float) xDiff));

            int xSum = 0;
            int ySum = 0;

            for (int i = 0; i < stepsRequired; i++) {
                if ((xSum == xDiff) && (ySum == yDiff)) { break; }
                int xDisp = (int)Math.ceil((maxDistPerUnitTime) * Math.cos(theta));
                int yDisp = (int)Math.ceil((maxDistPerUnitTime) * Math.sin(theta));


                if (xSum >= xDiff) {
                    xDisp =  0;
                }

                if (ySum >= yDiff) {
                    yDisp =  0;
                }

                if ((xDisp + yDisp) == 0) { break; }

                if (i == (stepsRequired - 1)) {
                    xDisp =  xDiff -  xSum;
                    yDisp = yDiff -  ySum;
                }

                // check if proposed with exceed then cope
                if ( (xSum + xDisp) > xDiff) {
                    xDisp -= ((xSum + xDisp) - xDiff);
                }
                if ( (ySum + yDisp) > yDiff) {
                    xDisp -= ((xSum + xDisp) - xDiff);
                    yDisp -= ((ySum + yDisp) - yDiff);
                }


                int[] currentLocation =  new int[] {xDisp * xMult, yDisp * yMult};


                journeySteps.add(new Integer[] {
                        currentLocation [0],
                        currentLocation [1],
                                xSum * xMult, ySum * yMult,
                                (xDiff -  xSum) * xMult, (yDiff - ySum) * yMult,
                                agXPos[agId], agYPos[agId], x, y
                        });
                xSum +=  Math.abs(currentLocation[0]);
                ySum +=  Math.abs(currentLocation[1]);

            }
        }
        return journeySteps;
    }


    /**
     * checks to see if there is an obstacle on the path between an agent and a specific location expressed as a displacement
     * @param agent     agent id
     * @param x         x-axis displacement
     * @param y         y-axis displacement
     * @return          returns the last habitable location before the obstacle or an empty array if there are no onstacles
     */
    private Integer[] hasObstacleOnPath (int agent, int x, int y) {

        ArrayList<Integer[]> journeyAsUnitSteps = splitJourneyBasedOnTimeSteps(agent, x, y, 1);


        boolean foundObstacle =  false;
        int safeIndex = -1;

        int rollingX =  agXPos[agent];
        int rollingY = agYPos[agent];

        for (int i = 0; i < journeyAsUnitSteps.size(); i++) {
            //Note: I am aware that a shorthand or oneliner can be used here....
            Integer[] entry = journeyAsUnitSteps.get(i);

            rollingX  +=  entry[0];
            rollingY  +=  entry[1];

            boolean hasObstacle = grid.hasItem(GridItem.OBSTACLE.getCode(), rollingX, rollingY);
            boolean hasAgent  = grid.hasItem(GridItem.AGENT.getCode(), rollingX, rollingY);
            boolean isBase =  grid.hasItem(GridItem.BASE.getCode(), rollingX, rollingY);
            boolean isMyLocation =  (agXPos[agent] == rollingX)  && (agYPos[agent] == rollingY);


            if ( (hasObstacle || hasAgent) && !(isBase || isMyLocation) ) {
                foundObstacle = true;
                safeIndex =  i - 1;
                break;
            }
        }


        if (foundObstacle) {
            if (safeIndex == -1) {
                return new Integer[]{1};
            } else {
                return journeyAsUnitSteps.get(safeIndex);
            }
        }

        return new Integer[]{} ;
    }



    /**
     *  scans the map for artefacts
     * @param agent     agent if
     * @param range     scan range
     * @return          action feedback
     */
    ActionFeedback scan (int agent, int range) {

        actedInCurrentCycle[agent] =  true;
        if (agRange[agent] >= range){

            if (range <= 0) {
                return new ActionFeedback(Action.SCAN.name, ActionStatusCode.INVALID_ACTION, new int[]{InvalidActionStatuscode.INVALID_PARAM.getCode()});
            }

            final int x =  agXPos[agent];
            final int y =  agYPos[agent];
            grid.addOrUpdateItemIfLarger(GridItem.SCAN_REGION.code, x, y, range);

            scanLocationX[agent] = x;
            scanLocationY[agent] = y;
            ArrayList<Integer> findings =  new ArrayList<>();

            ArrayList<Integer[]> obsInRange = getObstaclesInRegion(x, y, range);
            for (int i = (x - range); i <= (x + range); i++) {
                final int normalisedX = i < 0 ? grid.getWidth() + i : i >= grid.getWidth() ? i - grid.getWidth() : i;
                for (int j = (y - range); j <= (y + range); j++) {
                    final int normalisedY = j < 0 ? grid.getHeight() + j : j >= grid.getHeight() ? j - grid.getHeight() : j;

                    // Refactored for debugging
                    boolean withinVR = withinVisibleRange (i, j, x, y, range);
                    boolean isNotFree =  ! grid.isFree(normalisedX, normalisedY);
                    boolean isNotABase = ! grid.hasItem(GridItem.BASE.code, normalisedX, normalisedY);
                    boolean isNotObstructed = ! isObstructed(x, y, i, j,obsInRange );


                    if (withinVR && isNotFree && isNotABase && isNotObstructed) {
                        //Note: using non-normalised coordinates for obstacles check
                        int[] tileEntries =  grid.getItems(normalisedX, normalisedY);
                        int[] tileEntriesCount = grid.getItemsCount(normalisedX, normalisedY);
                        for (int jj = 0; jj < tileEntries.length; jj++) {
                            if (
                                    (tileEntries[jj] != GridItem.BASE.code)
                                            && (tileEntries[jj] != GridItem.AGENT.code)
                                            && (tileEntries[jj] != GridItem.SCAN_REGION.code)) {
                                findings.add(tileEntries[jj]);
                                findings.add(tileEntriesCount[jj]);
                                findings.add(normalisedX - x);
                                findings.add(normalisedY - y);
                            }
                        }
                    } /* else {
                        System.out.println("Location: " + normalisedX + "," +  normalisedY + " :: (vr, nfree, nab, ino): " +  withinVR + "," +  isNotFree + "," + isNotABase + "," +  isNotObstructed);
                    }*/
                }
            }

            deductEnergy(agent);
            if (findings.size() == 0) {
                return new ActionFeedback(Action.SCAN.name, ActionStatusCode.RESOURCE_NOT_FOUND);
            } else {
                int[] additionalInfo =  new int[findings.size()];
                for (int index = 0; index < findings.size(); index++) {
                    additionalInfo[index] =  findings.get(index);
                }
                return new ActionFeedback(Action.SCAN.name, ActionStatusCode.RESOURCE_FOUND, additionalInfo);
            }
        }
        return new ActionFeedback(Action.SCAN.name, ActionStatusCode.INVALID_ACTION, new int[]{InvalidActionStatuscode.UNMET_REQUIREMENT.getCode()});
    }


    /**
     *  checks to see if an item is within visible range of an agent
     * @param x       x-axis location
     * @param y       y-axis location
     * @param cx      x-axis location of scan origin
     * @param cy      y-axis location of scan origin
     * @param r       scan radius
     * @return        visibility
     */
    private boolean withinVisibleRange (int x, int y, int cx, int cy, int r) {
        int xDist =  Math.abs(cx - x);
        int yDist =  Math.abs(cy - y);
        double dist =  Math.abs(Math.sqrt( (xDist * xDist) + (yDist * yDist)));
        if (dist <= r) { return true; }
        return false;
    }


    /**
     * identifies all obstacles within a range
     * @param x         x-axis location of scan origin
     * @param y         y-axis location of scan origin
     * @param range     scan range
     * @return          list of obstacles discovered
     */
    public ArrayList<Integer[]> getObstaclesInRegion (int x, int y, int range) {
        //todo: modify this so that an obstacle can block the visibility of another obstacle...
        ArrayList<Integer[]> obstacles = new ArrayList<>();
        for (int i = (x - range); i <= (x + range); i++) {
            final int normalisedX = i < 0 ? grid.getWidth() + i : i >= grid.getWidth() ? i - grid.getWidth() : i;
            for (int j = (y - range); j <= (y + range); j++) {
                final int normalisedY = j < 0 ? grid.getHeight() + j : j >= grid.getHeight() ? j - grid.getHeight() : j;
                if (grid.hasItem(GridItem.OBSTACLE.code, normalisedX, normalisedY)){
                    obstacles.add(new Integer[] {i, j});
                }

            }
        }
        return obstacles;
    }



    /**
     * checks to see if a location is obstructed or not
     * @param x         origin x-axis
     * @param y         origin y-axis
     * @param cx        x-disp
     * @param cy        y-disp
     * @param obs       list of obstacles
     * @return          obstructed or not
     */
    private boolean isObstructed (int x, int y, int cx, int cy, ArrayList<Integer[]> obs) {
        if ((obs == null) || (obs.size() == 0)) { return false; }
        for (Integer[] i : obs) {
            // sot hat we can see the obstacle
            if ((cx ==  i[0]) && (cy == i[1])){ return false; }
            if (isBetweenPoints(x, y, cx, cy, i[0], i[1])) {
                //System.out.println("The obstacle: " + i[0] + "," +  i[1] + " blocks: " +  cx + "," + cy);
                return true;
            }
        }
        return false;
    }


    /**
     *
     * @param xStart
     * @param yStart
     * @param xStop
     * @param yStop
     * @param xCheck
     * @param yCheck
     * @return
     */
    private boolean isBetweenPoints (int xStart, int yStart, int xStop, int yStop, int xCheck, int yCheck){
       // enhances readability
        double distanceToEndpoint = calculateDistanceApart(xStart, yStart, xStop, yStop);
        double distanceToCheckpoint = calculateDistanceApart(xStart, yStart, xCheck, yCheck);
        // if it's further away then it cant be between...
        if (distanceToCheckpoint > distanceToEndpoint) { return false; }

        // check polarity
        // so that points on opposite side of start doesnt get flagged
        boolean polarityMatch =  (checkPolarity(xStart, xStop) == checkPolarity(xStart, xCheck)) && (checkPolarity(yStart, yStop) == checkPolarity(yStart, yCheck));


        // between is now defined as sharing gradient and being the check point being closer than
        // the end point...
        return (calculateGradient(xStart, yStart, xCheck, yCheck) == calculateGradient(xStart, yStart, xStop, yStop)) && polarityMatch ;
     }


    private double calculateGradient (int xStart, int yStart, int xStop, int yStop) {
        return (double) (yStop - yStart) / (double) (xStop - xStart);
    }



    /**
     * Collects a resource from a grid tile
     * @param agent     Agent performing the collection task
     * @return          Action execution feedback
     */
    ActionFeedback collectResource (int agent) {
        actedInCurrentCycle[agent] =  true;

        // confirms that the agent can carry resources and has sufficient space
       if ((agCapacity[agent] > 0) && (agResourceHeld[agent] < agCapacity[agent])) {
            // confirms that the sought after resource is present

           if (agPrefResource[agent] == GridItem.UNDEFINED.code){
               if (grid.hasItem(GridItem.GOLD.code, agXPos[agent], agYPos[agent])) {
                   agPrefResource[agent] = GridItem.GOLD.code;
                   System.out.println( agNames[agent] + ": now has a preferred type of Gold");
               } else if (grid.hasItem(GridItem.DIAMOND.code, agXPos[agent], agYPos[agent])) {
                   agPrefResource[agent] = GridItem.DIAMOND.code;
                   System.out.println( agNames[agent] + ": now has a preferred type of Diamond");
               }
           }

            if (grid.hasItem(agPrefResource[agent], agXPos[agent], agYPos[agent])) {
                // confirms that the agent has sufficient energy to collect the resource
                if (hasSufficientEnergy(agent, Action.COLLECT, new int[]{})) {
                    // updates the grid entry and the agent's registry
                    grid.reduceItem(agPrefResource[agent], agXPos[agent], agYPos[agent], 1);
                    agResourceHeld[agent] =  agResourceHeld[agent] + 1;
                    agResHeldType[agent] = agPrefResource[agent];
                    deductEnergy(agent);
                    return new ActionFeedback(Action.COLLECT.name, ActionStatusCode.ACTION_COMPLETED);
                }
                return new ActionFeedback(Action.COLLECT.name, ActionStatusCode.INSUFFICIENT_ENERGY, new int[] { (int) agEnergy[agent]});
            }
        }
        deductPenalty(agent, 10);
        return new ActionFeedback(Action.COLLECT.name, ActionStatusCode.INVALID_ACTION, new int[]{InvalidActionStatuscode.UNMET_REQUIREMENT.getCode()});
    }

    /**
     * Deposit a resource
     * @param agent     Agent performing deposit task
     * @return          Action execution feedback
     */
    ActionFeedback depositResource (int agent) {

        actedInCurrentCycle[agent] =  true;

        // confirms that the agent is carrying a resource
        if (agResourceHeld[agent] > 0) {
            // confirms that agent has sufficient energy
            if (hasSufficientEnergy(agent, Action.DEPOSIT, new int[]{})) {
                // figures out where the deposit is taking place
                if (!grid.hasItem(GridItem.BASE.code, agXPos[agent], agYPos[agent])) {
                    // place resource back on grid if the current location is not a base
                    grid.addItem(agResHeldType[agent], agXPos[agent], agYPos[agent], 1);
                } else {
                    collectedRes[agPrefResource[agent]] = collectedRes[agPrefResource[agent]] + 1 ;
                }
                agResourceHeld[agent] = agResourceHeld[agent] - 1;
                deductEnergy(agent);
                return new ActionFeedback(Action.DEPOSIT.name, ActionStatusCode.ACTION_COMPLETED);
            }
            return new ActionFeedback(Action.DEPOSIT.name, ActionStatusCode.INSUFFICIENT_ENERGY, new int[] { (int) agEnergy[agent]});
        }
       deductPenalty(agent, 10);

        return new ActionFeedback(Action.DEPOSIT.name, ActionStatusCode.INVALID_ACTION, new int[]{InvalidActionStatuscode.UNMET_REQUIREMENT.getCode()});
    }

    /**
     * Confirms that an Agent has sufficient energy to perform a task
     * @param agent     Agent about to perform task
     * @param action    Action the agent is about to perform
     * @param info      Context based information
     * @return          Energy sufficiency
     * @apiNote         info changes based on the action being performed
     *                  scanning: info =  new int[] {[scan_range]}
     *                  moving: info = new int[] { [speed], [x-displacement], [y-displacement]}
     *
     */
    private boolean hasSufficientEnergy (int agent, Action action, int[] info) {
        double val = 0;
        switch(action) {
            case COLLECT:
                val = 12.5;
                break;
            case DEPOSIT:
                val = 12.5;
                break;
            case MOVE:
                double distanceToTravel = caculateTrueDistance(info[0], info[1]);
                        //calculateDistanceApart(agXPos[agent], agYPos[agent], info[0], info[1]);
                val =  4.5 * distanceToTravel;
                break;
            case SCAN:
                val  = ( (double) info[0] /  (double) agRange[agent] )  * 15;
                break;
        }

        agRequiredEnergy[agent] =  val;
        return agEnergy[agent] >= val;
    }

    /**
     *
     * @param agent
     */
    private void deductEnergy (int agent) {
        agEnergy[agent] =  agEnergy[agent] - agRequiredEnergy[agent];
        if (agEnergy[agent] < 0) { agEnergy[agent] = 0; }
    }

    /**
     *
     * @param agent
     * @param val
     */
    private void deductPenalty (int agent, double val) {
        agEnergy[agent] =  agEnergy[agent] - val;
        if (agEnergy[agent] < 0) { agEnergy[agent] = 0; }
    }


    /**
     * Actions to perform once a resoning cycle ends
     */
    public void onReasoningCycleEnded () {
        //Note: do whatever you wish to do on the end of a reasoning cycle

        billIdleAgents();

        if (view != null) {
            view.setGeneralMessage(getResourceSummary() + getAgentsSummary());
            view.refresh();
        }
        if (endOfScenarioDetected()) {
            view.endSim(endOfScenarioReason());
            endListener.onEndConditionDetected(endOfScenarioReason());
        }

    }

    /***
     *
     */
    public void onReasoningCycleStarted () {
        clearScanRegionCache();
    }


    /**
     *
     * @return
     */
    private String getAgentsSummary () {
        StringBuilder summary = new StringBuilder("Agent Summary:-\n");
        for (int i = 0; i < agNames.length; i++) {
            summary.append("  " +  agNames[i] + ":\n");
            summary.append("   energy: " + (new DecimalFormat("#.##").format(agEnergy[i])).toString() + "\n");
            if (agResourceHeld[i] > 0){
                summary.append("   payload: " + agResourceHeld[i] + " " + GridItem.getLiteralString(agResHeldType[i]) );
            }
            summary.append("\n");

        }
        return summary.toString();
    }

    /**
     *
     * @return
     */
    private String getResourceSummary () {
        StringBuilder summary =  new StringBuilder("Deposited Resources:-\n");
        summary.append("Diamond: " + collectedRes[GridItem.DIAMOND.code] + " of "  +  availableRes[GridItem.DIAMOND.code] + "\n");
        summary.append("Gold:    " + collectedRes[GridItem.GOLD.code] +  " of " +  availableRes[GridItem.GOLD.code] + "\n" );
        summary.append("\n");
        return summary.toString();
    }

    /**
     *
     * @return
     */
    public boolean endOfScenarioDetected () {

        // check energy
        boolean allOutOfEnergy = true;
        for (int i = 0; i < agEnergy.length; i++) {
            allOutOfEnergy = allOutOfEnergy && (agEnergy[i]  == 0);
        }
        if (allOutOfEnergy) {
            terminationReason =  "All agents are out of energy";
            return true;
        }

        // check if all resources have been collected
        int collectedResourceCount = 0;
        for (int i = 0; i < collectedRes.length; i++) {
            collectedResourceCount += collectedRes[i];
        }

        if (collectedResourceCount >= totalResources) {
            terminationReason = "All resources collected";
            return true;
        }
        // TODO: check all agent energy and decide if remaining agents can handle the remaining tasks
        return false;
    }

    /**
     *
     * @return
     */
    public String endOfScenarioReason () {
        return terminationReason;
    }

    /**
     *
     */
    public static void destroy () {
        model = null;
    }


    /**
     * Action(s) to perform when the user changes the scan region persistence option
     * @param state
     */
    public void onPersistRegionToggled (boolean state) {
        persistScanRegion = state;
    }

    /**
     *
     * @param xPos
     * @param yPos
     */
    public void onTileSelected (int xPos, int yPos) {
        //TODO: implement efficiently.. perhaps make the grid do a lot of the work
    }

    /**
     *
     */
    private void clearScanRegionCache () {
        if (! persistScanRegion) {
            for (int i = 0; i < scanLocationX.length; i++) {
                try {
                    grid.removeItem(GridItem.SCAN_REGION.getCode(), scanLocationX[i], scanLocationY[i]);
                }catch (Exception ex){}
            }
        }

        scanLocationX =  new int[agCapacity.length];
        scanLocationY =  new int[agCapacity.length];
    }

    /**
     *
     */
    private void billIdleAgents () {
        for (int i = 0; i < agEnergy.length; i++) {
            if (! actedInCurrentCycle[i]){
                double energyLevel = agEnergy[i] - energyCostPerReasoningCycle;
                agEnergy[i] =  energyLevel > 0 ? energyLevel : 0;
            }
            actedInCurrentCycle [i] = false;
        }

    }




}
