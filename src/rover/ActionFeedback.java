package rover;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ActionFeedback {

    private final String action;
    private final RoverWorldModel.ActionStatusCode status;
    private final int[] extraInfo;

    /**
     * Returns the execution status code
     * @return  Returns action execution status code
     */
    public RoverWorldModel.ActionStatusCode getStatus () { return status; }

    public int[] getExtraInfo () { return extraInfo; }

    /**
     * Constructor
     * @param agentAction   Action performed by the agent
     * @param actionStatus  Status code of action execution
     * @param executionInfo Additional information for the context
     */
    public ActionFeedback (String agentAction, RoverWorldModel.ActionStatusCode actionStatus, int[] executionInfo) {
        action = agentAction;
        status = actionStatus;
        extraInfo = executionInfo;
    }

    /**
     * Constructor
     * @param agentAction Action performed by the agent
     * @param actionStatus  Status code of action execution
     */
    public ActionFeedback (String agentAction, RoverWorldModel.ActionStatusCode actionStatus) {
        action = agentAction;
        status = actionStatus;
        extraInfo = new int[]{};
    }

    /**
     * Returns the percept parameters
     * @return  Percept parameters
     */
    public String getFeedbackParams () {
        String params = "";
        switch (status) {
            case INSUFFICIENT_ENERGY:
                params = "(" + action + "," + extraInfo[0] + ")";
                break;
            case INVALID_ACTION:
                //TODO: place string val w/o hardcoding
                // hardcoded due to time constrain
                params = "(" + action + "," +  RoverWorldModel.InvalidActionStatuscode.getLiteralString(extraInfo[0]) + ")";
                break;
            case ACTION_COMPLETED:
                params = "(" +  action + ")";
                break;
            case MOVEMENT_OBSTRUCTED:
                params = "(";
                for (int i  =  0; i < extraInfo.length && i < 4; i++) {
                    params = params + extraInfo[i] + ",";
                }
                params = params.substring(0, params.length() -1) + ")";
                break;
            case RESOURCE_NOT_FOUND:
            default:
                params = "";
                break;
        }
        return params;
    }

    /**
     * Returns the percept parameters.
     * Note: This is useful for cases where more than one feedback info has been placed in the extra info array
     *
     * @param split no. that indicates what no. of entries belong to  single record
     * @return
     */
    public String[] getFeedbackParams (int split) {
        if (extraInfo.length < split) {
            return new String[]{ getFeedbackParams() };
        } else {
            switch (status) {
                case RESOURCE_FOUND:
                    boolean isDivisible = extraInfo.length % split == 0;
                    int count =  extraInfo.length / split;
                    int endPoint = isDivisible ? extraInfo.length : count * split;


                    String[] params =  new String[count];
                    for (int i = 0; i < endPoint; i += split) {
                        params [i / split] = "(";
                        for (int j = 0; j < split; j++) {
                            if (j == 0) {
                                params [i/ split] += "\"" +  RoverWorldModel.GridItem.getLiteralString(extraInfo[i + j]) + "\",";
                            } else {
                                params [i/ split] += extraInfo[i + j] + ",";
                            }

                        }
                        params[i / split] = params[i / split].substring(0, params[i / split].length() - 1) + ")";
                    }
                    // let's try and remove duplicates from here.... ideally should be handled from the model not here...
                    // snippet from: https://stackoverflow.com/questions/10366856/delete-duplicate-strings-in-string-array
                    params = new HashSet<String>(Arrays.asList(params)).toArray(new String[0]);

                    return params;
                default:
                    return new String[]{ getFeedbackParams() };
            }
        }
    }

}
