package rover;

/**
 * Read-only interface for the grid object
 */
interface ReadonlyGrid {
    int getCount (int item, int xPos, int yPos);
    int getHeight ();
    int[] getItems (int xPos, int yPos);
    int[] getItemsCount (int xPos, int yPos);
    int getWidth();
    boolean hasItem (int item, int xPos, int yPos);
    boolean isFree(int xPos, int yPos);
    void registerListener (GridChangeListener l);
    void unregisterListener (GridChangeListener l);
}
