package rover;

interface RoverWorldViewListener {
    void onPersistRegionToggled (boolean state);
    void onTileSelected (int xPos, int yPos);
}
