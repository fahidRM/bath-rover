package rover.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Term;

public class clear_map extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args)
            throws Exception {
        System.out.println("CLEARED MAP: " + ts.getUserAgArch().getAgName() );
        MapBuilder.getInstance(ts.getUserAgArch().getAgName()).clearMap();
        return true;
    }
}