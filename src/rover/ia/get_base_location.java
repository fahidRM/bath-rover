// Internal action code for project rover_cw

package rover.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;

public class get_base_location extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args)
            throws Exception {
        final NumberTerm xVal = new NumberTermImpl(MapBuilder.getInstance(ts.getUserAgArch().getAgName())
                .getBaseLocation().x);
        final NumberTerm yVal = new NumberTermImpl(MapBuilder.getInstance(ts.getUserAgArch().getAgName())
                .getBaseLocation().y);
        return un.unifies(xVal, args[0]) && un.unifies(yVal, args[1]);
    }
}