package rover.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.StringTerm;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
import rover.RoverWorld;

public class get_config extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args)
            throws Exception {
        // structure intended for flexible usage.
        // i.e we would limit output to the no. of params passed
        final String ag =  ts.getUserAgArch().getAgName();
        boolean resp  = true;

        // fly me to the moon

        if (args.length >= 1) {
            un.unifies(new NumberTermImpl(RoverWorld.getCapacity(ag)), args[0]);
        }
        if (args.length >= 2) {
            un.unifies(new NumberTermImpl(RoverWorld.getSpeed(ag)), args[2]);
        }
        if (args.length >= 3) {
            un.unifies(new NumberTermImpl(RoverWorld.getScanRange(ag)), args[1]);
        }
        if (args.length >= 4) {
            resp &= un.unifies(new StringTermImpl(RoverWorld.getResourceType(ag)), args[3]);
        }
        return resp;
    }
}