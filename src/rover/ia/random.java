package rover.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;

import java.util.Random;

public class random extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args)
            throws Exception {

        final Random r = new Random();
        final int val = r.nextInt(9);

        final NumberTerm rVal = new NumberTermImpl(val);
        return un.unifies(rVal, args[0]);
    }
}