package rover.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
import rover.RoverWorld;

public class get_energy extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args)
            throws Exception {
        final NumberTerm energy = new NumberTermImpl(RoverWorld.getEnergy(ts
                .getUserAgArch().getAgName()));
        return un.unifies(energy, args[0]);
    }
}