package rover.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Term;

public class add_journey extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args)
            throws Exception {
        try {
            final int x = (int) ((NumberTerm) args[0]).solve();
            final int y = (int) ((NumberTerm) args[1]).solve();
            MapBuilder.getInstance(ts.getUserAgArch().getAgName()).addJourney(x, y);
            System.out.println("Log: " + ts.getUserAgArch().getAgName() +  ":- " + x + ", " + y);
        } catch (final Throwable t) {
            t.printStackTrace();
            return false;
        }
        return true;
    }
}