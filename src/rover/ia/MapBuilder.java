package rover.ia;

import jason.environment.grid.Location;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class is made package-public as it is only required by the internal
 * actions. ==> To share information through a single instance
 * */

class MapBuilder {

    private static HashMap<String, MapBuilder> instances = null;
    private ArrayList<Location> locations = null;

    /**
     * Returns instance of Map Builder
     *
     * @return
     */
    public static MapBuilder getInstance(String ag) {
        if (instances == null) {
            instances =  new HashMap<String, MapBuilder>();
            instances.put(ag, new MapBuilder());
        }
        if (instances.get(ag) == null) {
            instances.put(ag, new MapBuilder());
        }
        return instances.get(ag);
    }

    private MapBuilder() {
        clearMap();
    }

    /**
     * Clears the map
     */
    public void clearMap() {
        locations = new ArrayList<Location>();
    }

    /**
     * Adds a journey to the list
     *
     * @param x
     *            x displacement
     * @param y
     *            y displacement
     */
    public void addJourney(int x, int y) {
        locations.add(new Location(x, y));
    }

    /**
     * Returns the location on the Base
     *
     * @return Location of the Base
     */
    public Location getBaseLocation() {
        int x = 0;
        int y = 0;
        for ( Location l : locations) {
            x += l.x;
            y += l.y;
        }


        return new Location(-1 * x, -1 * y);
    }

}
