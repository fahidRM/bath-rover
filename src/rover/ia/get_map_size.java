package rover.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
import rover.RoverWorld;

public class get_map_size extends DefaultInternalAction{
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args)
            throws Exception {

        final int[] size =  RoverWorld.getMapSize();
        final NumberTerm width =  new NumberTermImpl(size[0]);
        final NumberTerm height = new NumberTermImpl(size[1]);

        return un.unifies(width, args[0]) && un.unifies(height, args[1]);
    }
}
